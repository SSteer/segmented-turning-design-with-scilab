function RE_Demos(win)
  mainH=findobj("figure_id", win)

  subdemolist=[
      _("TS","Bowl")                "Bowl.ts"
      _("TS","Bowl with border")    "Bowl2.ts" 
      _("TS","Bowl with pattern")   "Basket2.ts" 
      _("TS","Bowl with zigzag")    "Bowlzz.ts" 
      _("TS","Open basket")         "OpenBasket2.ts" 
      _("TS","Fruit bowl")          "FruitBowl.ts" 
      _("TS","Vase with pattern")   "Pixels.ts" 
      _("TS","Bol with staves")     "Staves.ts" 
      _("TS","Pencil Pot")          "PencilPot.ts"];
  n=x_choose(subdemolist(:,1),_("TS","Choose a demonstration"))
  if n<>0 then
    RE_Load(win,TS_Path()+"/demos/"+subdemolist(n,2))
  end
endfunction
