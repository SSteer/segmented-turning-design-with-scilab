function h=CreateEdit(parent,x,y,w,h,init,tag,callback)
    h=uicontrol( ...
        "parent"              , parent,...
        "style"               , "edit",...
        "relief"              , "groove",...
        "string"              , init,...
        "units"               , "pixels",...
        "position"            , [x,y,w,h],...
        "fontname"            , defaultfont,...
        "fontunits"           , "points",...
        "fontsize"            , 12,...
        "horizontalalignment" , "left", ...
        "BackgroundColor"     , [1 1 1], ...
        "ForegroundColor"     , [1 1 1]*0, ...
        "callback_type",10,...
        "callback"            , callback, ...
        "scrollable"          , "on",...
        "tag"                 , tag,...
        "visible"             , "on");
endfunction
