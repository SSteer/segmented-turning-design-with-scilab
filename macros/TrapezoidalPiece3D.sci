function [S,M]=TrapezoidalPiece3D(Ri_z,Re_z,z,Ai,Ae,C)
//Computes the 3D representation of the turned  segment piece 
//(segment or spacer)

//z is the discretization of the elevation of a ring
//Ri_z is the associated abscissae of the interior of the ring
//Re_z is the associated abscissae of the exterior of the ring
//A is the miter angle
//C is the piece color
//S struct containing the facets
//M  struct containing the mesh of the piece
  n=size(Re_z,"*");
  if size(Ai,2)==1 then
    m=max(2,ceil(50*max(Ae)/%pi));
    alphae=linspace(-Ae,Ae,m);// nxm
    alphai=linspace(-Ai,Ai,m);
  else
    alphae=Ae;
    alphai=Ai;
    m=size(alphae,2);
  end
  ce=cos(alphae);se=sin(alphae);
  ci=cos(alphai);si=sin(alphai);
   //----------------------------------------------------------
  //Facettes
  //----------------------------------------------------------

  X=[];Y=[];Z=[];
  for j=1:m-1
    for i=1:n-1
      //facette exterieure
      X=[X [Re_z(i)*ce(i,j);
            Re_z(i+1)*ce(i+1,j);
            Re_z(i+1)*ce(i+1,j+1);
            Re_z(i)*ce(i,j+1)]];
      Y=[Y [Re_z(i)*se(i,j);
            Re_z(i+1)*se(i+1,j);
            Re_z(i+1)*se(i+1,j+1);
            Re_z(i)*se(i,j+1)]];
      Z=[Z [z(i);z(i+1);z(i+1);z(i)]]; 
      //facette interieure
      X=[X [Ri_z(i)*ci(i,j);
            Ri_z(i+1)*ci(i+1,j);
            Ri_z(i+1)*ci(i+1,j+1);
            Ri_z(i)*ci(i,j+1)]];
      Y=[Y [Ri_z(i)*si(i,j);
            Ri_z(i+1)*si(i+1,j);
            Ri_z(i+1)*si(i+1,j+1);
            Ri_z(i)*si(i,j+1)]];
      Z=[Z [z(i);z(i+1);z(i+1);z(i)]];
      //facette superieure
      X=[X [Re_z(i+1)*ci(i+1,j);
            Ri_z(i+1)*ci(i+1,j);
            Ri_z(i+1)*ci(i+1,j+1);
            Re_z(i+1)*ce(i+1,j+1)]];
      Y=[Y [Re_z(i+1)*se(i+1,j);
            Ri_z(i+1)*si(i+1,j);
            Ri_z(i+1)*si(i+1,j+1);
            Re_z(i+1)*se(i+1,j+1)]]; 
      Z=[Z z(i+1)*ones(4,1)]; 
      //facette inferieure
      X=[X [Re_z(i)*ce(i,j);
            Ri_z(i)*ci(i,j);
            Ri_z(i)*ci(i,j+1);
            Re_z(i)*ce(i,j+1)]];
      Y=[Y [Re_z(i)*se(i,j);
            Ri_z(i)*si(i,j);
            Ri_z(i)*si(i,j+1);
            Re_z(i)*se(i,j+1)]]; 
      Z=[Z z(i)*ones(4,1)]; 
      //facette droite 
      X=[X [Re_z(i)*ce(i,j+1);
            Ri_z(i)*ci(i,j+1);
            Ri_z(i+1)*ci(i+1,j+1);
            Re_z(i+1)*ce(i+1,j+1)]];
      Y=[Y [Re_z(i)*se(i,j+1);
            Ri_z(i)*si(i,j+1);
            Ri_z(i+1)*si(i+1,j+1);
            Re_z(i+1)*se(i+1,j+1)]]; 
      Z=[Z [z(i);z(i);z(i+1);z(i+1)]]; 
      //facette droite gauche
      X=[X [Re_z(i)*ce(i,j);
            Ri_z(i)*ci(i,j);
            Ri_z(i+1)*ci(i+1,j);
            Re_z(i+1)*ce(i+1,j)]];
      Y=[Y [Re_z(i)*se(i,j);
            Ri_z(i)*si(i,j);
            Ri_z(i+1)*si(i+1,j);
            Re_z(i+1)*se(i+1,j)]]; 
      Z=[Z  [z(i);z(i);z(i+1);z(i+1)]]; 
    end
  end
  S=struct("X",X,"Y",Y,"Z",Z,"C",C*ones(1,(n-1)*(m-1)*6))
 
  //----------------------------------------------------------
  //Mesh
  //----------------------------------------------------------
   X=[];Y=[];Z=[];
   //facette exterieure
  xf=[Re_z($)*ce($,:)'; //m
      Re_z($:-1:1).*ce($:-1:1,$); //n
      Re_z(1)*ce(1,$:-1:1)' //m
      Re_z.*ce(:,1)]; //n
  yf=[Re_z($)*se($,:)';
      Re_z($:-1:1).*se($:-1:1,$);
      Re_z(1)*se(1,$:-1:1)'
      Re_z.*se(:,1)];
  zf=[z($)*ones(m,1);
      z($:-1:1);
      z(1)*ones(m,1)
      z];
  X=[X xf];Y=[Y yf];Z=[Z zf];

  //facette interieure    
  xf=[Ri_z($)*ci($,:)';
      Ri_z($:-1:1).*ci($:-1:1,$);
      Ri_z(1)*ci(1,$:-1:1)'
      Ri_z.*ci(:,1)];
  yf=[Ri_z($)*si($,:)';
      Ri_z($:-1:1).*si($:-1:1,$);
      Ri_z(1)*si(1,$:-1:1)'
      Ri_z.*si(:,1)];
  zf=[z($)*ones(m,1);
      z($:-1:1);
      z(1)*ones(m,1)
      z];
  X=[X xf];Y=[Y yf];Z=[Z zf];

  //facette superieure
  xf=[Re_z($)*ce($,:)';
      linspace(Re_z($)*ce($,$),Ri_z($)*ci($,$),n)';
      Ri_z($)*ci($,$:-1:1)';
      linspace(Ri_z($)*ci($,1),Re_z($)*ce($,1),n)'];
  yf=[Re_z($)*se($,:)';
      linspace(Re_z($)*se($,$),Ri_z($)*si($,$),n)';
      Ri_z($)*si($,$:-1:1)';
      linspace(Ri_z($)*si($,1),Re_z($)*se($,1),n)'];
  zf=z($)*ones(2*n+2*m,1);
  X=[X xf];Y=[Y yf];Z=[Z zf];
  
  //facette inferieure
  xf=[Re_z(1)*ce(1,:)';
      linspace(Re_z(1)*ce(1,$),Ri_z(1)*ci(1,$),n)';
      Ri_z(1)*ci(1,$:-1:1)';
      linspace(Ri_z(1)*ci(1,1),Re_z(1)*ce(1,1),n)'];
  yf=[Re_z(1)*se(1,:)';
      linspace(Re_z(1)*se(1,$),Ri_z(1)*si(1,$),n)';
      Ri_z(1)*si(1,$:-1:1)';
      linspace(Ri_z(1)*si(1,1),Re_z(1)*se(1,1),n)'];
  zf=z(1)*ones(2*n+2*m,1);
  X=[X xf];Y=[Y yf];Z=[Z zf];

  //facette droite
  xf=[Re_z.*ce(:,$);//n
      linspace(Re_z($)*ce($,$),Ri_z($)*ci($,$),m)';
      Ri_z($:-1:1).*ci($:-1:1,$);
      linspace(Ri_z(1)*ci(1,$),Re_z(1)*ce(1,$),m)'];
  yf=[Re_z.*se(:,$);
      linspace(Re_z($)*se($,$),Ri_z($)*si($,$),m)';
      Ri_z($:-1:1).*si($:-1:1,$);
      linspace(Ri_z(1)*si(1,$),Re_z(1)*se(1,$),m)'];
  zf=[z;
      z($)*ones(m,1);
      z($:-1:1);
      z(1)*ones(m,1)];
  X=[X xf];Y=[Y yf];Z=[Z zf];
 
    //facette gauche
  xf=[Re_z.*ce(:,1);
      linspace(Re_z($)*ce($,1),Ri_z($)*ci($,1),m)';
      Ri_z($:-1:1).*ci($:-1:1,1);
      linspace(Ri_z(1)*ci(1,1),Re_z(1)*ce(1,1),m)'];
  
  yf=[Re_z.*se(:,1);
      linspace(Re_z($)*se($,1),Ri_z($)*si($,1),m)';
      Ri_z($:-1:1).*si($:-1:1,1);
      linspace(Ri_z(1)*si(1,1),Re_z(1)*se(1,1),m)'];
  
  zf=[z;z($)*ones(m,1);z($:-1:1);z(1)*ones(m,1)];
  X=[X xf];Y=[Y yf];Z=[Z zf];
  M=struct("X",X,"Y",Y,"Z",Z)
endfunction
