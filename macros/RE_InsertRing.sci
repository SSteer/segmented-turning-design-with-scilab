function RE_InsertRing(win)
  if type(win)==1 then
    mainH=findobj("figure_id", win);
  else
    mainH=win;
  end
  ud=mainH.user_data;
  D=ud.D;
  Handles=ud.H;
  Heights=D.H;
  h=x_mdialog(_("TS","New ring height"),_("TS","Height mm"),"20");
  if h==[] then return;end
  if execstr("h=["+strcat(h,";")+"]","errcatch")<>0 then 
    messagebox(lasterror(),"error","error","modal");
    return;
  end
  l=Handles.index.value;
  Heights=[Heights(1:l) h Heights(l+1:$)];
  if sum(Heights(1:$-1))>max(D.Pe.y) then
     messagebox(_("TS","Last ring is above the workpiece"),...
                  "warning","warning","modal");
  end
  RP=D.RingsProperties;
  RP=[RP(1:l) default_rings_properties(1) RP(l+1:$)];
  D.RingsProperties=RP;
  D.H=Heights;
  D=Profile2Rays(D);
  RE_Update(mainH,D)
endfunction
