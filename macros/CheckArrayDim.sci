function [r,v]=CheckArrayDim(bounds,h)
//Checks the answer given in Edit uicontrol
//It should be evaluated as an array of integer values  in 
//[bounds(1), bounds(2)] interval
  if argn(2)==1 then h=gcbo;end
  if execstr("v=(["+h.string+"])","errcatch")<>0 then
    RE_SetError(h,%t,_("TS","Expression cannot be evaluated"))
    r=%f,v=[]
  else
    if argn(2)<1 then
      RE_SetError(h,%f)
      r=%t
    else
      n=size(v,"*")
      r=and(n>=bounds(1))&and(n<=bounds(2));
      if r then
        RE_SetError(h,~r)//un hilite edit area
      else
        msg=msprintf(_("TS","Expected dimensions must be in the interval [%g %g]"),bounds(1),bounds(2))
        RE_SetError(h,~r,msg)
      end
    end
  end
endfunction
