function PE_Load(win,path)
//Filemenu callback for the "Load ..." sub menu  
//It is used to load a previously generated profile
  if type(win)==1 then
    mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  ud=mainH.user_data
  
  if ud.pile<>[] then
    r=messagebox([_("TS","The current design has not been saved")
                  _("TS","Do you really want to erase it?")],"message","question",[_("TS","Ok"),_("TS","No")],"modal")
    if r ==2 then return;end
  end
 
  if argn(2)<2 then
    path=uigetfile("*.pf")
    if path=="" then return,end
  end

  if execstr("load(path)","errcatch")<>0|exists("ze","local")==0|exists("zi","local")==0|exists("thickness","local")==0 then 
    messagebox(_("TS","Incorrect file"),"modal")
    return
  end
  ud.Path=path
  ud.Active=1
  ud.pile=[]
  ud.thickness=thickness
  H=max(imag(ze));
  D=max(real(ze))*2;
  ud.D=D
  ud.H=H
  sz=get(0, "screensize_px")(3:4);
  mainH.figure_size=(sz(2)-60)*[D/2/H 1];
  ax=gca();
  ax.data_bounds=[-1 -1;D/2 H];
  C=ud.C
  C(1).data=[real(ze) imag(ze)];
  C(5).data=[real(zi) imag(zi)];
  x=real(ze)
  y=imag(ze)
 
//  zz=bezier(zi,20,'o')(:);
//  ud.C(2).data=[real(zz) imag(zz)];
  PE_Update(ud.C)
  mainH.user_data=ud
  mainH.figure_name="Profile editor (%d) :"+fileparts(path,"fname");
  mainH.event_handler_enable="on";
endfunction
