function a=zigzag(Segments,Layers)
  thickness=Layers.thickness
  thickness=[thickness($:-1:1) thickness(2:$)];
  C=Layers.colors
  h=Segments(1);l=Segments(2)
  zh=Layers.zheight
  dh=(h-zh)/2
  C=[C($:-1:1)' C(2:$)']
  e=sum(thickness(2:$-1))
  t=(e*zh+l*sqrt(zh^2+l^2-e^2))/(zh^2+l^2);
  a=atan((zh*t-e)/l,t);

  n=size(thickness,"*")
  h1=e/cos(a);
  if thickness(1)<l*sin(a) then
    error(msprintf(_("TS","last thickness is to small <%g"),l*sin(a)));
  end
  ax=gca();ax.isoview="on";ax.axes_visible="on";//ax.margins=zeros(1,4);
  drawlater()
  c=colors(C)
   x=[];y=[];
  //left
  y0=-thickness(1)/cos(a);
  y1=y0+l*tan(a)
  for k=1:n
    h1=thickness(k)/cos(a);
    x=[x;   0 l    l   0  0]; 
    y=[y; [y0 y1 y1+h1 y0+h1 y0]];
    y0=y0+h1;
    y1=y1+h1;
  end
  //right
  y0=-thickness(1)/cos(a);
  y1=y0+l*tan(a)
 
  for k=1:n
    h1=thickness(k)/cos(a);
    x=[x;   l+[l 0 0 l  l]];
    y=[y; [y0 y1 y1+h1 y0+h1 y0]];
    y0=y0+h1;
    y1=y1+h1;
  end
  
  xfpolys(x',y',[c c])
  ax.data_bounds=[min(x) min(y); max(x) max(y)];
  //cuts
  xc=[0 0;
       2*l 2*l];
  yc=[y(2,1)-dh h-dh 
      y(2,1)-dh h-dh];
 
  xpolys(xc,yc,color("red")*[1 1])
  drawnow()
  return
  
  
  
  subplot(212)
  ax=gca();ax.isoview="on";ax.data_bounds=[0 -h+h1; 2*l 2*h-h1];ax.margins=zeros(1,4);


  o=[l/2,h/2];
  l1=l/cos(a);
  for k=4:6
    cc=copy(e1.children(k),ax);
    [cc.data(:,1),cc.data(:,2)]=Rot2D(cc.data(:,1),cc.data(:,2),-a,o);
    cc=copy(cc);
    cc.data(:,1)=cc.data(:,1)+l1+3;
  end

 drawnow()
 Ht=h*cos(a)+l*sin(a);//hauteur totale de la planche laminee
 printf("H1=%g,e=%g,L=%g\n",((h+l1*sin(a))*cos(a)-e)/2,e,l1)
endfunction

