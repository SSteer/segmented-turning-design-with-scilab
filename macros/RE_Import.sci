function RE_Import(win)
//Filemenu callback for the "Import Profile ..." sub menu  

  if type(win)==1 then
    mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  if mainH.user_data.Edited==%t then
    r=messagebox([_("TS","The current design has not been saved")
                  _("TS","Do you really want to erase it?")],"message","question",[_("TS","Ok"),_("TS","No")],"modal")
    if r ==2 then return;end
  end
  
  path=uigetfile("*.pf")
  if path=="" then return,end

  if execstr("load(path)","errcatch")<>0 then
    messagebox(_("TS","Incorrect file"),"modal")
    return
  end
  vnames=listvarinfile(path)
 
  if and(gsort(vnames)==["zi";"ze";"thickness"]) then
    zz=bezier(ze,40,'o');
    Pe=struct("x",real(zz),"y",imag(zz),"ze",ze)
    if zi==[] then
      Pi=InteriorProfile(Pe,thickness)
    else
      zz=bezier(zi,40,'o');
      x=real(zz);y=imag(zz);
      if x(1)>0 then 
        x=[0 x];y=[y(1) y];
      elseif x(1)<0 then 
        x(1)=0
      end
      Pi=struct("x",x,"y",y,"zi",zi)
    end
  elseif and(gsort(vnames)==["thickness";"Pi";"Pe"]) then
    if Pi==[] then Pi=InteriorProfile(Pe,thickness);end
  else    
    messagebox(_("TS","Incorrect file"),"modal")
    return
  end
  D=struct("Re",[],"Ri",[],"H",[],"Pe",Pe,"Pi",Pi,"Ewidth",0,"RingsProperties",[]);
  Path=strsubst(path,".pf",".ts")
  
  RE_Defaults(mainH,D,Path)

endfunction
