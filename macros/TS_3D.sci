function [S,M]=TS_3D(D,turned,view)
//computes and shows (if required) the 3D representaion of the full
//workpiece either raw (turned=%f or omitted) or  turned (turned=%t)
  if argn(2)==1 then
    turned=%f
  elseif and(turned<>[%t %f]) then
    error(msprintf(_("%s: Wrong type for argument #%d: Boolean expected.\n"),"TS_3D",2))
   
  end
  if argn(2)<3 then
    view=%f
  elseif and(view<>[%t %f]) then
     error(msprintf(_("%s: Wrong type for argument #%d: Boolean expected.\n"),"TS_3D",3))
  end
  N=D.RingsProperties.N;
  R=D.RingsProperties.Rotation;
  r=cumsum(R.*(2*%pi./N(:)));
  if argn(2)==1|turned==%f then 
    Ring=SegmentedRing3D;
    cm=2
  else 
    Ring=Ring3D;
    cm=2//-2 //mesh non displayed
  end
  
  S=list();M=list();
  for l=1:size(D.Ri,2)
    [Sl,Ml]=Ring(D,l)
    for i=1:size(Sl)
      [Sl(i).X,Sl(i).Y]=Rot2D(Sl(i).X,Sl(i).Y,r(l));
      [Ml(i).X,Ml(i).Y]=Rot2D(Ml(i).X,Ml(i).Y,r(l));
    end
    S=lstcat(S,Sl);
    M=lstcat(M,Ml);
  end    

  if view then
    ax=gca();delete(ax.children);
    Surf3D(S,M)
    ax.rotation_angles=[85 45];
    xtitle("")
  end
endfunction
