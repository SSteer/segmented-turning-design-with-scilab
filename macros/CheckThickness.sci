function [r,v]=CheckThickness(ZigzagProps,N,H,Re)
//Checks the answer given in Edit uicontrol
//It should be evaluated as an array of integer values  in 
//[bounds(1), bounds(2)] interval
  h=gcbo
  if execstr("v=(["+h.string+"])","errcatch")<>0 then
    RE_SetError(h,%t,_("TS","Expression cannot be evaluated"))
    r=%f,v=[]
  elseif type(v)<>1|~isreal(v)|or(v<=0) then
    RE_SetError(h,%t,_("TS","Real array expected"))
    r=%f
  elseif size(v,"*")<2 then
    msg=msprintf(_("TS","Expected dimensions must be in the interval [%g,  %g]"),2,%inf)
    RE_SetError(h,%t,msg)
    r=%f
  else
    zheight=ZigzagProps.zheight
    e=2*sum(v(2:$-1))+v(1);
    r=e<=zheight;
    if r then
      RE_SetError(h,~r)//unhilite edit area
    else
      n=size(v,"*")
      msg=msprintf(_("TS","thickness(1)+2*sum(thickness(2:%d))=%g must be less than %s %g"),...
                   n-1,e,_("TS","Zigzag height"),zheight)
      RE_SetError(h,~r,msg)
    end
  end
endfunction
