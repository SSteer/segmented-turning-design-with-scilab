function RE_UpdateProfiles(win)
  if type(win)==1 then
    mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  ud=mainH.user_data
  D=ud.D
  ze=D.Pe.ze
  if ze==[] then
    x=D.Pe.x(:);y=D.Pe.y(:);
    xd=x($);yd=y($)
    x=x(1:10:$);y=y(1:10:$)
    if y($)<yd then x($+1)=xd;y($+1)=yd;end
    ze=(x+%i*y)
  end
  zi=D.Pi.zi
  if zi==[] then
    x=D.Pi.x(:);y=D.Pi.y(:);
    xd=x($);yd=y($)
    x=x(1:10:$);y=y(1:10:$)
    if y($)<yd then x($+1)=xd;y($+1)=yd;end
    zi=(x+%i*y)
  end
 
  guiHandle=ProfileEditor(mainH);
  ud=guiHandle.user_data
  C=ud.C
  scf(guiHandle)
  H=max(imag(ze));
  D=max(real(ze))*2;
  ud.D=D
  ud.H=H
  sz=get(0, "screensize_px")(3:4);
  guiHandle.figure_size=(sz(2)-60)*[D/2/H 1];
  ax=gca();
  ax.data_bounds=[-1 -1;D/2 H];
  C(1).data=[real(ze) imag(ze)];
  C(6).data=[real(zi) imag(zi)];
  x=real(ze)
  y=imag(ze)
  zz=bezier(zi,20,'o')(:);
  C(4).data=[real(zz) imag(zz)];
  ud.C=C
  PE_Update(ud.C)
  guiHandle.user_data=ud
  //guiHandle.figure_name="Profile editor (%d) :"+fileparts(path,"fname");
  guiHandle.event_handler_enable="on";
end
