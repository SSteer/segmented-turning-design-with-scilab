function Surf3D(S,M)
  ax=gca();
  for i=1:size(S)
    Xm=M(i).X
    //draw facettes
    X=S(i).X;Y=S(i).Y;Z=S(i).Z;C=S(i).C
     if Xm==[] then
       cm=2;//edges are drawn
     else 
       cm=-2;//edges are not drawn
     end
    plot3d(X,Y,list(Z,C));e=gce();
    e.hiddencolor=0;e.color_mode=cm;
    if Xm<>[] then
      //draw mesh
      Ym=M(i).Y;Zm=M(i).Z;
      xpolys3D(Xm,Ym,Zm)
    end
  end

  ax.isoview="on";
  
endfunction
