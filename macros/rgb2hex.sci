function h=rgb2hex(rgb)
  //converts an [R G B] array with values in [0 1] into a string of
  //hexadecimal color representatio
 
  w=dec2hex(round(255*rgb))
  k=find(length(w)==1)
  if k<>[] then  w(k)="0"+w(k);end
  h="#"+strcat(w,"","c")
endfunction
