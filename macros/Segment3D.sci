function [X,Y,Z]=Segment3D(Ri,Re,N,e,GapProps)
//Computes the 3D coordinates of a segment according the the ring properties
//Ri interior radius
//Re exterior radius
//N number of segments in the ring,
//GapProps: ring properties

  if argn(2)<5 then
    [X2d,Y2d]=Segment2D(Ri,Re,N)
  else
    [X2d,Y2d]=Segment2D(Ri,Re,N,GapProps)
  end
  X=[];Y=[];Z=[];
  for k=1:size(X2d,2)
    x2d=X2d(1:4,k);
    y2d=Y2d(1:4,k);
    
    //Facette basse
    X=[X x2d];
    Y=[Y y2d];
    Z=[Z [0;0;0;0]];
    
    ///Facette haute
    X=[X x2d];
    Y=[Y  y2d];
    Z=[Z e+[0;0;0;0]];
    
    //Facette interieure
    X=[X [x2d(1);x2d(1);x2d(2);x2d(2)]];
    Y=[Y [y2d(1);y2d(1);y2d(2);y2d(2)]];
    Z=[Z [0;e;e;0]];
   
    //Facette exterieure  
    X=[X  [x2d(3);x2d(3);x2d(4);x2d(4)]];
    Y=[Y [ y2d(3);y2d(3);y2d(4);y2d(4)]];
    Z=[Z [0;e;e;0]];
    
    //Facette +A
    X=[X [x2d(3);x2d(3);x2d(2);x2d(2)]];
    Y=[Y [y2d(3);y2d(3);y2d(2);y2d(2)]];
    Z=[Z [0;e;e;0]];
    
    //Facette -A
    X=[X [x2d(4);x2d(4);x2d(1);x2d(1)]];
    Y=[Y [y2d(4);y2d(4);y2d(1);y2d(1)]];
    Z=[Z [0;e;e;0]];
    
  end

endfunction
