function PE_SetProfile(win)
  if type(win)==1 then
     mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  ud=mainH.user_data

  C=ud.C;
  C(3).data=C(2).data;//non edited profile shown as reference

  d=C(1).data;
  C(1).data=C(5).data;//shows the knots of the profile to be edited
  C(5).data=d; //save knots curve of the currently edited profile
  
  
  if ud.Active==1 then //Exterior --> Interior
    ud.Active=2 
  else //Interior -->Exterior 
    ud.Active=1 
  end
  if C(1).data==[] then
    C(1).data=[0 ud.thickness]
  end
  zz=bezier(C(1).data*[1;%i],40,'o');
  C(2).data=[real(zz);imag(zz)]'
  ud.pile=[]
  ud.C=C
  mainH.user_data=ud
endfunction
