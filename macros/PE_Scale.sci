function PE_Scale(win)
  if type(win)==1 then
     mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  [ok, sx,sy]=getvalue(_("TS","Workpiece scale factors"),...
                       [_("TS","Diameter scale");_("TS","Height scale")],...
                       list("vec",1,"vec",1),["1";"1"])
  if ~ok then return;end
  ud=mainH.user_data
  ud.D=ud.D*sx
  ud.H=ud.H*sy
  
  for k=1:6
    c=ud.C(k)
    c.data=c.data*diag([sx,sy])
  end
  mainH.user_data=ud
endfunction
