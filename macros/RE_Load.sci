function RE_Load(win,path)
//Filemenu callback for the "Load ..." sub menu  
//It is used to load a previously generated workpiece data structure
  if type(win)==1 then
    mainH=findobj("figure_id", win)
  else
    mainH=win
  end
   if mainH.user_data.Edited==%t then
    r=messagebox([_("TS","The current design has not been saved")
                  _("TS","Do you really want to erase it?")],"message","question",[_("TS","Ok"),_("TS","No")],"modal")
    if r==2 then return;end
  end
  
  if argn(2)==1 then 
    path=uigetfile("*.ts");
    if path=="" then return,end
  end
  
  if execstr("load(path)","errcatch")<>0|exists("D","local")==0|typeof(D)<>"st"//|D.type<>"TS" then 
    messagebox(_("TS","Incorrect file"),"modal");
    return
  end
  Edited=%f;
  //upward compatibility
  RP=D.RingsProperties;
  if size(RP.N,1)==1 then 
    RP.N=RP.N(:)
    D.RingsProperties=RP;
    Edited=%t;
  end  
 
  if and(fieldnames(RP)<>"StavesProps") then
    st=list();
    for l=1:size(RP.N,"*")
       st($+1)=struct("Re",[],"Ri",[]);
    end
    RP=mlist(["rp","N","Rotation","Type","Colors","SpacerColor","GapProps" ...
              "ZigzagProps","StavesProps"],..
             RP.N,RP.Rotation,RP.Type,RP.Colors,RP.SpacerColor,RP.GapProps,..
             RP.ZigzagProps,st);
    D.RingsProperties=RP;
    Edited=%t;
  end
  if and(fieldnames(D.Pe)<>"ze") then
    D.Pe.ze=[];
    D.Pi.zi=[];
    Edited=%t;
  end
  if and(fieldnames(D)<>"Version") then
    
    RP=D.RingsProperties
    GP=RP.GapProps
    N=RP.N
    for k=1:size(N,'*')
      if GP(k).gap>0&GP(k).gap<1 then
        //convert gap_angle into gap_width
        Edited=%t;
        gap_angle=GP(k).gap*2*%pi/N(k);
        GP(k).gap=round(20*D.Re(k)*sin(gap_angle/2))/10;
      end
      
    end
    D.RingsProperties.GapProps=GP;
    D.Version=[1 2];
  end
   
 
  Handles=mainH.user_data.H;
  HDisplay=Handles.Display;
  HDisplay.value=find(HDisplay.string==_("TS","Turned workpiece"));
  
  Handles.index.string=string(1:size(D.H,"*"))';Handles.index.value=1
  mainH.user_data.Path=path;
  mainH.user_data.Edited=Edited;
  RE_Update(mainH,D);
  
  mainH.figure_name="Ring editor (%d) :"+fileparts(path,"fname");
endfunction
