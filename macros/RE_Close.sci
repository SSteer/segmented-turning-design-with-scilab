function RE_Close(win)
//Filemenu callback for the "Close" sub menu
   if type(win)==1 then
    guiHandle=findobj("figure_id", win)
  else
    guiHandle=win
  end
  if guiHandle.user_data.Edited==%t then
    r=messagebox([_("TS","The design has not been saved")
                  _("TS","Do you really want to close the editor?")],"message","question",[_("TS","Ok"),_("TS","No")],"modal")
    if r ==2 then return;end
  end
  delete(guiHandle)
endfunction
