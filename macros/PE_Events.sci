function PE_Events(win, xcli, ycli, ibut)
  guiHandle=scf(win);
  ud=guiHandle.user_data;
  ax=guiHandle.children(2);
  [xcli,ycli]=xchange(xcli,ycli,'i2f');
  guiHandle.info_message=msprintf("%.4g, %.4g",xcli,ycli)
  C=ud.C ;
  pile=ud.pile
  x=C(1).data(:,1);y=C(1).data(:,2);
  n=size(x,'*');
  if or(ibut==[1122 1090]) then //CTRL-Z undo
    exec(PE_Undo,-1)
    return
  elseif  or(ibut==[1115 1083]) then //CTRL-S save
    PE_Save(win,%f)
    return
  elseif  or(ibut==[1079 1111]) then //CTRL-O load
    PE_Load(win)
    return
  elseif ud.move<>[] then
    l=ud.move(1)
    x(l)=xcli;y(l)=ycli
    op="Drag"
    PE_Update(C)
    if ibut==-5 then //end move
      l=ud.move(1)
      x(l)=xcli;y(l)=ycli
      ud.pile=[pile;2,l, ud.move(2),ud.move(3)];
      ud.move=[]
      guiHandle.user_data=ud
    end
    return
  end
  

  if and(ibut<>[-4 0 1 3 4 10 11 ]) then return;end
 
 
 
  e=0.0001;
  dy=diff(ax.data_bounds(:,2))
  dx=diff(ax.data_bounds(:,1))
  
  [d,ls]=min(((ycli-y)./dy).^2+((xcli-x)./dx).^2)

  if d<e then //a knot has been selected
    if or(ibut==[1 4]) then //suppression
      pile=[pile;3,ls x(ls),y(ls)];
      x(ls)=[];y(ls)=[];
      op="Middle click to delete the knot"
      PE_Update(C)
     
    elseif ibut==0 then
      ud.move=[ls,x(ls),y(ls)]
      guiHandle.user_data=ud
    end
    return
  end
  [d,ptp,l,c] = orthProj([x,y],[xcli ycli])
  //mprintf("d=%s,ptp=%s,l=%s,c=%s\n",sci2exp(d),sci2exp(ptp),sci2exp(l),sci2exp(c))
 
  ls=[]
  if l==[] then 
    if ycli<y(1) then
      if ((ycli-y(1))/dy)^2+((xcli-x(1))/dx)^2<e then
        ls=1
      else //add an initial point
        x=[xcli;x];
        y=[ycli;y] ;
        ax.data_bounds(1,:)=min(ax.data_bounds(1,:),[xcli ycli]-2);
        pile=[pile;1 0 xcli ycli];
        op="Click to add a knot"
        PE_Update(C)
      end
    else 
      if ((ycli-y($))/dy)^2+((xcli-x($))/dx)^2<e then
        ls=n
      else//add a final point
        x=[x;xcli];
        y=[y;ycli]; 
        ax.data_bounds(2,:)=max(ax.data_bounds(2,:),[xcli ycli]+2);
        pile=[pile;1,size(x,"*") xcli ycli];
        op="Click to add a knot"
        PE_Update(C)
      end
    end
  elseif ((ycli-y(l))/dy)^2+((xcli-x(l))/dx)^2<e then
    ls=l
  elseif ((ycli-y(l+1))/dy)^2+((xcli-x(l+1))/dx)^2<e then
    ls=l+1
  else //insertion d'un point
    x=[x(1:l);xcli;x(l+1:$)];
    y=[y(1:l);ycli;y(l+1:$)] ;
    op="Click to insert a knot"
    PE_Update(C)
    pile=[pile;1,l xcli ycli];
  end
  if ls<>[] then
    if or(ibut==[1 4]) then //suppression
      pile=[pile;3,ls x(ls),y(ls)];
      x(ls)=[];y(ls)=[];
      op="Middle click to delete the knot"
      PE_Update(C)
      return
    elseif ibut==0 then
      ud.move=[ls,x(ls),y(ls)]
    end
  end
  ud.pile=pile
  guiHandle.user_data=ud
endfunction

function PE_Undo()
  if pile<>[] then
    l=pile($,2)
    select pile($,1)
    case 1 //undo add
      if l==0 then
        x=x(2:$);y=y(2:$);
      else
        x(l+1)=[];y(l+1)=[];
      end
    case 2 //undo move
      x(l)=pile($,3);y(l)=pile($,4);
    case 3 //undo delete
      if l>n then
        x=[x;pile($,3)]
        y=[y;pile($,4)]
      elseif l==1 then
        x=[pile($,3);x]
        y=[pile($,4);y]
      else
        x=[x(1:l-1);pile($,3);x(l:$)]
        y=[y(1:l-1);pile($,4);y(l:$)]
      end
    end
    pile($,:)=[];
    op="CTRL-Z to Undo";
    PE_Update(C)
  end
   ud.pile=pile
   guiHandle.user_data=ud
endfunction
