function PE_Save(win,opt)
//Filemenu callback for the "Save" and "Save As ..." sub menus  
//It is used to save the current workpiece daa structure
  if type(win)==1 then
     mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  ud=mainH.user_data
  if argn(2)<2|opt then
    path=uiputfile("*.pf")
    if path=="" then return,end
  else
    path=ud.Path
  end
  C=ud.C
  if ud.Active==1 then //Exterior 
    ze=C(1).data*[1;%i];
    zi=C(5).data*[1;%i];
  else //Interior 
    zi=C(1).data*[1;%i];
    ze=C(5).data*[1;%i];
  end
 
  thickness=ud.thickness
  
  save(path,"ze","zi","thickness")
  ud.pile=[]
  ud.Path=path
  mainH.user_data=ud
  mainH.figure_name=_("TS","Profile editor")+" (%d) :"+fileparts(path,"fname");
endfunction
