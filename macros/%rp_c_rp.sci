function a=%rp_c_rp(a,b)
  fn=fieldnames(a)';
  for f=fn
    if type(a(f))==15 then
      a(f)=lstcat(a(f),b(f));
    else
      a(f)=[a(f);b(f)];
    end
  end
endfunction
