function Pi=InteriorProfile(Pe,e)
//computes the interior profile diven the exterior profile  in D.Pe and
//the wall thickness e. e must be either a scalar (constant thickness) or
//a row array with same size as Pe.x
  
  [me,ne]=size(e)
  if me*ne<>1 then
    if me*ne<>size(Pe.x,2) then
      error(msprintf(_("TS","%s:#%d argument must be a scalar or an array with same size as #%d argument"),"InteriorProfile",1,2))
    end
    e=matrix(e,1,-1)
  end
  
  
  x=Pe.x;y=Pe.y;
  
  dx=diff(x);dy=diff(y);
  dx($+1)=dx($);dy($+1)=dy($);
  nrm=sqrt(dx^2+dy^2);
  xi=x-(dy./nrm).*e;
  yi=y+(dx./nrm).*e;
  xi(1)=max(xi(1),0)
  //modification des points  limites
  yi($)=max(y)
  k=find(yi<e(1)&xi<=0)
  xi(k)=[];yi(k)=[];
 
  //suppression des zones de rebroussement
  while %t
    dy=diff(yi);
    k=find(dy<=0);if k==[] then break;end
    k=unique([k k+1]);
    yi(k)=[];xi(k)=[];
  end
  //force origin
  yi=[e(1) yi];
  xi=[0 xi]
  Pi=struct("x",xi,"y",yi,"zi",[]); 
endfunction
