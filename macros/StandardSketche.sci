function StandardSketche(D,lr)
  RP=D.RingsProperties(lr);
  N=RP.N;
  A=%pi/N;
  GapProps=RP.GapProps(1);
  typ=GapProps.type;
  gap_width=GapProps.gap;
  ngap=GapProps.ngap;
  filled=GapProps.filled;
  H=D.H(lr); 
  Re=D.Re(lr)+D.Ewidth;
  Ri=max(0,D.Ri(lr)-D.Ewidth);

  wx=0.2*H;
  wy=wx;
  yshift=5*wy;
  xshift=5*wx;
  
  [PP,AA]=SegmentPoints(A,Ri,Re,gap_width);
 
  xo=3*wx
  if ngap==0|gap_width==0|typ=="full"  then
    E=norm(AA(:,3)-AA(:,2));
    Le=norm(PP(:,2)-PP(:,3));
    Li=norm(PP(:,1)-PP(:,4));
    gca().data_bounds=[0 -E-yshift-2*wy;Le H+2*wy];
    SegSketch(xo,Le,Li,E,H,_("TS","Segments"))
  elseif ngap==N then
    E=norm(AA(:,3)-AA(:,2));
    Le=norm(PP(:,8)-PP(:,6));
    Li=norm(PP(:,7)-PP(:,5));
    Ef=norm(PP(:,6)-PP(:,5));
    xo2=xo+Le+xshift
    xbnd=xo2
    if filled then  xbnd=xbnd+xshift+gap_width;end
    gca().data_bounds=[0 -E-yshift-2*wy;gap_width H+2*wy];
    SegSketch(xo,Le,Li,E,H,_("TS","Segments"))
    if filled then fillersketch(xo2,gap_width,Ef,H,_("TS","Filler"));end
  else
    //segments near space/filler
    xo1=xo;
    Le1=norm(PP(:,8)-PP(:,3));
    Li1=norm(PP(:,7)-PP(:,4));
    E=norm(AA(:,3)-AA(:,2));
   
    xo2=xo1+Le1+xshift
    
    if N>2*ngap then
      //regular segments
      Le2=norm(PP(:,2)-PP(:,3));
      Li2=norm(PP(:,1)-PP(:,4));
      xo3=xo2+Le2+xshift
    else
      xo3=xo2
    end
    xbnd=xo3
    if filled then  xbnd=xbnd+xshift+gap_width;end
    gca().data_bounds=[0 -E-yshift-2*wy;xbnd H+2*wy];

    SegSketch(xo1,Le1,Li1,E,H,_("TS","Segments around spacer"))
    if N>2*ngap then SegSketch(xo2,Le2,Li2,E,H,_("TS","Regular Segments"));end;
    if filled then 
      Ef=norm(PP(:,6)-PP(:,5));
      fillersketch(xo3,gap_width,Ef,H,_("TS","Filler"));
    end
  end
  s=_("TS","Outside view")
  r=xstringl(0,0,s,8,2)
  xstring(2*wx,(H-r(3))/2,s,-90)
  e=gce();e.font_style=8;e.font_size=2;
 
  s=_("TS","Top view")
  r=xstringl(0,0,s,8,2)
  xstring(2*wx,-yshift-E+(E-r(3))/2,s,-90)
  e=gce();e.font_style=8;e.font_size=2;
endfunction

function SegSketch(xo,Le,Li,E,H,tit)
//Segment
 
  //vue de l'exterieur
  x=Le*[0;1;1;0;0];
  y=[ H; H; 0;  0;  H ];
  xpoly(xo+x,y);gce().thickness=2;
  x=Le/2+Li/2*[-1;1;1;-1;-1]
  xpoly(xo+x,y);gce().line_style=3;gce().thickness=2;
  cotev([xo+Le,0],wx,H,"mm")
  coteh([xo+0 0],-wy,Le,"mm")
 
  //vue de dessus 
  x=[0;Le;(Le+Li)/2;(Le-Li)/2;0];
  y=-E*[1;1;0;0;1]
  xpoly(xo+x,y-yshift);gce().thickness=2;
  cotev([xo+Le,min(y)-yshift],wx,E,"mm")
  coteh([xo+(Le-Li)/2 -yshift],wy,Li,"mm")
  cotea([xo -E-yshift],[0 %pi/2-A],min(E,wx*0.8))
 
  //axes 
  xpoly(xo+Le/2*[1;1],[-E-yshift-2*wy; H+2*wy]);gce().line_style=4;
  //titre
  r=xstringl(0,0,tit,8,2)
  xstring(xo+(Le-r(3))/2,H+wy,tit)
  e=gce();e.font_style=8;e.font_size=2;
endfunction

function fillersketch(xo,gap_width,Ef,H,tit)
     //vue de l'exterieur
    x=gap_width*[0;1;1;0;0];
    y=[ H; H; 0;  0;  H ];
    xpoly(xo+x,y);gce().thickness=2;
    coteh([xo 0],-wy,gap_width,"mm")
    //vue de dessus
    x=[0;gap_width;gap_width;0;0];
    y=-Ef*[1;1;0;0;1]
    xpoly(xo+x,y-yshift);gce().thickness=2;
    cotev([xo+gap_width,min(y)-yshift],wx,Ef,"mm")
    //titre
    r=xstringl(0,0,tit,8,2)
    xstring(xo+(gap_width-r(3))/2,H+wy,tit)
    e=gce();e.font_style=8;e.font_size=2;
    
endfunction

