function PE_Close(win)
//Filemenu callback for the "Close" sub menu
   if type(win)==1 then
     guiHandle=findobj("figure_id", win)
   else
     guiHandle=win
   end
   pile=guiHandle.user_data.pile
   mainH=guiHandle.user_data.caller
   if pile<>[] then
     if mainH==[] then
       r=messagebox([_("TS","The design has not been saved")
                     _("TS","Do you really want to close the editor?")],...
                    "message","question",[_("TS","Ok"),_("TS","No")],"modal")
       if r ==2 then return;end
     else //ProfileEditor has been called  by RingEditor to update profiles
       r=messagebox([_("TS","The design has not been applied")
                     _("TS","Do you really want to close the editor?")],...
                    "message","question",[_("TS","Ok"),_("TS","No")],"modal")
       if r ==2 then return;end
     end
   end
 
  delete(guiHandle)
endfunction
