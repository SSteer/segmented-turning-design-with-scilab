function RE_RemoveRing(win)
  if type(win)==1 then
    mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  ud=mainH.user_data
  D=ud.D
  
  Handles=ud.H
  l=Handles.index.value;
  Heights=D.H;Heights(l)=[];
  
  if sum(Heights)<max(D.Pe.y) then
    messagebox(msprintf(_("TS","Sum of ring heights (%g mm) is less than profile height (%g mm)"),..
                        sum(Heights),round(max(D.Pe.y))),"warning","warning","modal")
  end
  Re=D.Re; Re(l)=[];D.Re=Re;
  Ri=D.Ri; Ri(l)=[];D.Ri=Ri;
 
  RP=D.RingsProperties;
  RP(l)=[];
  D.RingsProperties=RP;
  D.H=Heights;
  D=Profile2Rays(D);
  Handles.index.value=1;
  RE_Update(mainH,D)
endfunction
