function PE_Update(C)
  C(1).data=[x y];
  z=complex(x,y);
  if size(z,"*")>2 then
    zi=bezier(z,40,'o')(:);
    C(2).data=[real(zi),imag(zi)];
  end
endfunction
