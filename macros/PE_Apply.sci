function PE_Apply(win)
  //ProfileEditor has been called  by RingEditor to update profiles
  if type(win)==1 then
    guiHandle=findobj("figure_id", win)
  else
    guiHandle=win
  end
  ud=guiHandle.user_data
 
  C=ud.C
  if ud.Active==1 then //Exterior 
    ze=C(1).data*[1;%i];
    zi=C(5).data*[1;%i];
  else //Interior 
    zi=C(1).data*[1;%i];
    ze=C(5).data*[1;%i];
  end
  thickness=ud.thickness
  zz=bezier(ze,40,'o');
  Pe=struct("x",real(zz),"y",imag(zz),"ze",ze)
  if zi==[] then
    Pi=InteriorProfile(Pe,thickness)
  else
    zz=bezier(zi,40,'o');
    x=real(zz);y=imag(zz);
    if x(1)>0 then 
      x=[0 x];y=[y(1) y];
    elseif x(1)<0 then 
      x(1)=0
    end
    Pi=struct("x",x,"y",y,"zi",zi)
  end
  mainH=ud.caller
  scf(mainH)
  D=mainH.user_data.D
 
  ph=max(D.Pe.y)
  Heights=D.H
  r=max(Pe.y)/ph
  D.H=round(Heights*r)
  D.Pe=Pe
  D.Pi=Pi
  D=Profile2Rays(D);
  mainH.user_data.D=D
  mainH.user_data.Edited=%t;
  scf(mainH);
  RE_Update(mainH,D);
  mainH.immediate_drawing="on";
  delete(guiHandle)
endfunction
