function coteh(o,dy,l,unit)
  s=msprintf("%g",round(10*abs(l))/10)+unit;
  r=xstringl(0,0,s,6,1);w=r(3);
  yo=o(2)+dy
  xo=o(1)
  xpoly(xo+[0 0],o(2)+[0 1.5*dy])
  xpoly(xo+[0 0]+l,o(2)+[0 1.5*dy])
 
  xpoly(xo+[0 l],yo+[0 0])
  e=gce();e.line_style=3;e.polyline_style=4;
  xpoly(xo+[0.01 0],yo+[0 0])
  e=gce();e.line_style=3;e.polyline_style=4;
  if abs(l)>w then
    xstring(xo+(l-w)/2,yo,s);
  else
    xstring(xo+l,yo,s); 
  end
  
endfunction
