function PE_ShowDefaultProfile(win) 
  if type(win)==1 then
    mainH=findobj("figure_id", win);
  else
    mainH=win;
  end
  ud=mainH.user_data
  C=ud.C
  if C(4).data<>[] then
    mainH.children(1).children(2).Label=_("TS","Show default interior profile ")
    C(4).data=[]
  else
    if ud.Active==1 then
      ze=C(1).data*[1;%i]
    else
      ze=C(6).data*[1;%i]
    end
    zz=bezier(ze,40,'o');
    Pe=struct("x",real(zz),"y",imag(zz),"ze",ze)
    Pi=InteriorProfile(Pe,ud.thickness)
    C(4).data=[Pi.x;Pi.y]';
    mainH.children(1).children(2).Label=_("TS","Hide default interior profile ")
  end
endfunction
