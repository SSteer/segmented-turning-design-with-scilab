function RE_Cuts(win)
//Filemenu callback for the "Export cuts" sub menu  
  mainH=findobj("figure_id", win)
  D=mainH.user_data.D  
  if exists("xlwrite") then
    path=uiputfile(["*.txt";"*.xls";"*.xlsx"])
  else
    path=uiputfile("*.txt")
  end
  TS_Cuts(D,path,2)
endfunction
