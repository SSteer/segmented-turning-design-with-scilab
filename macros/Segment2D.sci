function [X,Y]=Segment2D(Ri,Re,N,GapProps)
//Computes the 2D coordinates of a segment according the the ring properties
//Ri interior radius
//Re exterior radius
//N number of segments in the ring,
//GapProps: ring properties

  if argn(2)<4 then
    opt="full"
    n=0;gap_width=0;
  else
    opt=convstr(GapProps.type)
    //  filled=GapProps.filled
    ngap=GapProps.ngap
    gap_width=GapProps.gap
    filled=GapProps.filled
  end

  A=%pi/N;
  PP=SegmentPoints(A,Ri,Re,gap_width)';
  
  X=[];Y=[];
  
  if gap_width==0|ngap==0|opt=="full" then
    [X,Y]=Rot2D(PP([1:4 1],1),PP([1:4 1],2),-A)
  else 
    //Rectangular space/spacer 
    if ngap==N then
      if (2*gap_width>Ri) then
        error("Gap width too large with respect to Ri")
      end
      [X,Y]=Rot2D(PP([5 6 8 7 5],1),PP([5 6 8 7 5],2),-A)
     
      if filled then
        //Add the spacer shape 
        [x,y]=Rot2D(PP([5 6 6 5 5],1),diag([1 1 -1 -1 1])*PP([5 6 6 5 5],2),-A)
        X=[X x];
        Y=[Y y];
      end
      [X,Y]=Rot2D(X,Y,A,[0 0]);
    else //3 segments : upon space, space,below space 
      //segment upon space
      Xu=PP([1 2 6 5 1],1);
      Yu=PP([1 2 6 5 1],2);
      //segment above space
      [Xa,Ya]=Rot2D(PP([3 4 7 8 3],1),PP([3 4 7 8 3],2),-2*A)
      
      
      if filled then
        Xs=PP([5 6 6 5 5],1);
        Ys=diag([1 1 -1 -1 1])*PP([5 6 6 5 5],2);
      else
        Xs=[];Ys=[];
      end
      X=[Xu Xs Xa];Y=[Yu Ys Ya];
    end
  end
endfunction
