function [SplayAngle,MiterCutAngle,TiltCutAngle]=StaveAngles(A,dR,H)
//N:numberofstaves
//SplayAngle: Outward tilt angle of staves (degrees)
//https://suncatcherstudio.com/woodturning/segmented-wood-turning/

  SplayAngle=abs(atan(dR,H));
  MiterCutAngle=atan(sin(SplayAngle).*tan(A));//Miter angle
  //TiltCutAngle=atan(ones(SplayAngle)*tan(A).*cos(atan(tan(SplayAngle)*(1./cos(A)))))
  w=sin(A).*cos(SplayAngle)
  TiltCutAngle=(atan(w./sqrt(1-w.^2)))
 
endfunction
