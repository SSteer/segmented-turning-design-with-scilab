function RE_Unroll(D)
  Re=D.Re
  D.H= D.H./Re

  ax=gca();
  drawlater()
  ax.view="2d";
  ax.axes_visible=["off" "off"];
 
  ax.margins=zeros(1,4)
  ax.isoview="off"
  
  for l=1:size(Re,"*")
    UnrollRing(D,l)
  end
   ax.data_bounds=[-%pi 0;%pi sum(D.H)];
  drawnow()
endfunction

function H=UnrollRing(D,lr)
  Re=D.Re(lr);
  Ri=D.Ri(lr);

  RP=D.RingsProperties(lr)
  rot=sum(D.RingsProperties.Rotation(1:lr));
  
  N=RP.N;
  A=%pi/N;
  Le=2*A
  nshift=int(rot)
  dx=(rot-nshift)*2*%pi/N
  x=-%pi+dx
  ind=pmodulo(-nshift+(0:N-1),N)+1
 
  H=D.H(lr)
  Segs=[];Labels=[]
  if or(RP.Type==["Standard" "Staves"]) then
    GapProps=RP.GapProps(1);
    C=RP.Colors(1)
    typ=convstr(GapProps.type)
    gap=GapProps.gap
    ngap=GapProps.ngap
    y=sum(D.H(1:lr-1));
    filled=GapProps.filled
    if ngap==0|gap==0|typ=="full"  then
      for k=1:N
        xfpoly(x+[0;Le;Le;0],y+[0;0;H;H],colors(C(ind(k))))
        e=gce();e.user_data=ind(k)
        Segs=[Segs e];
        Labels=[Labels drawnumber(x+Le/2,y+H/2,ind(k))]
        x=x+Le
      end
    elseif(typ=="rectangle"&ngap==N)  then
      Le_g=gap*2*A
      Le=(1-ngap*gap/N)*2*A
      for k=1:N
        xfpoly(x+[0;Le;Le;0],y+[0;0;H;H],colors(C(ind(k))))
        e=gce();e.user_data=ind(k)
        Segs=[Segs e];
        Labels=[Labels drawnumber(x+Le/2,y+H/2,ind(k))]
        x=x+Le
        if modulo(k,N/ngap)==0 then
          if filled then
            xfpoly(x+[0;Le_g;Le_g;0],y+[0;0;H;H],colors(RP.SpacerColor))
            e=gce(); e.user_data=0
            Segs=[Segs e];
          end
          x=x+Le_g
        end
      end
    else
      //rectangular spacer   
      Le_g=gap*2*A
      ind1=1:N/ngap:N
      ind2=modulo(ind1,N)+1
      for k=1:N
        if or(ind(k)==ind1) then
          xfpoly(x+[0;Le-Le_g/2;Le-Le_g/2;0],y+[0;0;H;H],colors(C(ind(k))))
          e=gce(); e.user_data=ind(k)
          Segs=[Segs e];
          Labels=[Labels drawnumber(x+(Le-Le_g/2)/2,y+H/2,ind(k))]
          x=x+Le-Le_g/2
          if filled then
            xfpoly(x+[0;Le_g;Le_g;0],y+[0;0;H;H],colors(RP.SpacerColor))
            e=gce();e.user_data=0
            Segs=[Segs e];
          end
          x=x+Le_g
        elseif or(ind(k)==ind2) then
          xfpoly(x+[0;Le-Le_g/2;Le-Le_g/2;0],y+[0;0;H;H],colors(C(ind(k))))
          e=gce();e.user_data=ind(k)
          Segs=[Segs e];
          Labels=[Labels drawnumber(x+(Le-Le_g/2)/2,y+H/2,ind(k))]
          x=x+Le-Le_g/2
        else
          xfpoly(x+[0;Le;Le;0],y+[0;0;H;H],colors(C(ind(k)))) 
          xrect(x,y,Le,H)
          e=gce();e.user_data=ind(k)
          Segs=[Segs e];
          Labels=[Labels drawnumber(x+Le/2,y+H/2,ind(k))]
          x=x+Le
        end
      end
    end
  else
    ZigzagProps=RP.ZigzagProps(1)
    thickness=ZigzagProps.thickness/Re
    thickness=[thickness($:-1:1) thickness(2:$)];
    C=colors(ZigzagProps.colors);
    zh=ZigzagProps.zheight/Re
    dh=(H-zh)/2
    e=sum(thickness(2:$-1))
    t=(e*zh+Le*sqrt(zh^2+Le^2-e^2))/(zh^2+Le^2);
    az=atan((zh*t-e)/Le,t);
    C=[C($:-1:1)';C(2:$)']
    y=sum(D.H(1:lr-1))
    n=size(thickness,"*");
     //left segment
    xl=[];yl=[];
    y0=-thickness(1)/cos(az);
    y1=y0+Le*tan(az)
    h1=thickness(1)/cos(az);
    //couche basse
    xl=[xl [0;   Le;   Le;    0]]; 
    yl=[yl [-dh;-dh;y1+h1;y0+h1]];
    y0=y0+h1;
    y1=y1+h1;
    for k=2:n-1
      h1=thickness(k)/cos(az);
      xl=[xl [0 ;Le;  Le;    0 ]]; 
      yl=[yl [y0;y1;y1+h1;y0+h1]];
      y0=y0+h1;
      y1=y1+h1;
    end
    h1=thickness(n)/cos(az);
    //face avant
    xl=[xl [0 ;Le;  Le;   0]]; 
    yl=[yl [y0;y1;H-dh;H-dh]];
    
    
    yl=yl+dh
    
    yr=yl
    xr=Le-xl

    x=-%pi+dx
    for k=1:N/2
      xfpolys(x+xl,y+yl,C); Segs=[Segs gce()]
      Labels=[Labels drawnumber(x+Le/2,y+H/2,ind(2*k-1))]
      x=x+Le
      xfpolys(x+xr,y+yr,C); Segs=[Segs gce()]
      Labels=[Labels drawnumber(x+Le/2,y+H/2,ind(2*k))]
      x=x+Le; 
    end
  end
  
  Segs=glue(Segs)
  if Labels<>[] then glue(Labels);end
  endfunction
