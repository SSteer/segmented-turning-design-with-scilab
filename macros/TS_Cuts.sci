function TS_Cuts(D,fn,t)
//Generates a csv file with description of all cuts
  if argn(2)<3 then t=2;end
  with_stave=%f
  for l=1:size(D.H,'*')
    if D.RingsProperties(l).Type=="Staves" then 
      with_stave=%t;
    end
  end
  xls_export=or(fileparts(fn,"extension")==[".xls" ".xlsx"])
  //miter angle (Cut angle degree) blade tilt for staves
  head_std=[_("TS","Ring index"),...
            _("TS","Wood type"),...
            _("TS","Exterior diameter"),...
            _("TS","Number of segments"),...
            _("TS","Exterior length mm"),...
            _("TS","Miter angle degree"),...
            _("TS","Fence position mm"),...
            _("TS","Board''s height mm"),...
            _("TS","Board''s width mm"),...
            _("TS","Board''s length mm")];
  
  
  head_staves=[_("TS","Ring index"),...
               _("TS","Wood type"),...
               _("TS","Max exterior diameter"),...
               _("TS","Number of segments"),...
               _("TS","Staves'' max width mm"),...
               _("TS","Miter angle degree"),...
               _("TS","Blade tilt degree"),...
               _("TS","Splay angle degree"),...
               _("TS","Board'' height mm"),...
               _("TS","Board''s thickness mm"),...
               _("TS","Board''s length mm")]      

  T_std=[]
  T_staves=[]
  for l=1:size(D.H,'*')
    RP=D.RingsProperties(l);
    H=D.H(l);

    select RP.Type
    case "Standard" then
      Re=D.Re(l)+D.Ewidth;
      Ri=max(0,D.Ri(l)-D.Ewidth);
      
      T_std=[T_std;StandardCuts(l,Re,Ri,H,RP)];
    case "Zigzag" then
      Re=D.Re(l)+D.Ewidth;
      Ri=max(0,D.Ri(l)-D.Ewidth);
      T_std=[T_std;ZigzagCuts(l,Re,Ri,H,RP)];
    case "Staves" then 
     
      T_staves=[T_staves;StavesCuts(l,H,RP)]
    end
  end
  T=[];
  if xls_export then //xls export
       if T_std<>[] then
      T=[T;
         num2cell(head_std)
         T_std]
    end
    if T_staves<>[] then
      if T_std<>[]  then T($+1,1)={[]};end
      n=size(head_staves,2)
      T($+1,[1 n])={_("TS","Staves"),[]}
      T=[T;
         num2cell(head_staves)
         T_staves]
    end
    deletefile(fn);
    xlwrite(fn,T)
  else//csv export
    if T_std<>[] then
      T=[T;
         strcat(head_std,";")
         T_std]
    end
    if T_staves<>[] then
      if T_std<>[]  then T=[T;""];end
      T=[T;_("TS","Staves")]
      
      T=[T;
         strcat(head_staves,";")
         T_staves]
    end
    deletefile(fn);
    mputl(T,fn)
  end
endfunction

