function StaveSketche(D,lr)
  //https://suncatcherstudio.com/woodturning/segmented-wood-turning/
   RP=D.RingsProperties(lr);
   N=RP.N;
   A=%pi/N;  
   StavesProps=RP.StavesProps(1);
   GapProps=RP.GapProps(1);
   typ=GapProps.type;
   gap_width=GapProps.gap;
   ngap=GapProps.ngap;
   filled=GapProps.filled;
   
   H=D.H(l);
   Re=StavesProps.Re+D.Ewidth;
   Ri=max(0,StavesProps.Ri-D.Ewidth/cos(A));
   //astuce pour assurer que les parois de la douve sont paralleles
   dR=max(Re-Ri)
  
   Ri=Re-dR
   [SplayAngle,MiterCutAngle,TiltCutAngle]=StaveAngles(A,diff(Re),H);
    
   [PPh,AAh]=SegmentPoints(A,Ri(2),Re(2),gap_width);
   [PPl,AAl]=SegmentPoints(A,Ri(1),Re(1),gap_width);
   Ef=norm(PPh(:,6)-PPh(:,5))*cos(SplayAngle);
   E=norm(AAh(:,3)-AAh(:,2))*cos(SplayAngle);
 
   h1=H/cos(SplayAngle);h2=E*tan(SplayAngle);
   height=h1+h2;
 

   //clf()
   wx=0.12*height;
   wy=wx;
   yshift=5*wy;
   xshift=5*wx;
   
   xo=4*wx;
   if ngap==0|gap_width==0|typ=="full"  then
      Le=[norm(PPh(:,2)-PPh(:,3));
           norm(PPl(:,2)-PPl(:,3))]
      Li=[norm(PPh(:,1)-PPh(:,4));
           norm(PPl(:,1)-PPl(:,4))];
    
      gca().data_bounds=[0            -E-yshift-2*wy;
                         max(Le)+5*wx height+2*wy];
      SegSketch(xo,Le,Li,E,H,_("TS","Segments"))
   elseif ngap==N then
     Le=[norm(PPh(:,8)-PPh(:,6));
         norm(PPl(:,8)-PPl(:,6))];
     Li=[norm(PPh(:,7)-PPh(:,5));
         norm(PPl(:,7)-PPl(:,5))];
     xo2=xo+max(Le)+xshift
     xbnd=xo2
     if filled then  xbnd=xbnd+xshift+gap_width;end
     gca().data_bounds=[0         -E-yshift-2*wy;
                        xbnd       height+2*wy];
     SegSketch(xo,Le,Li,E,H,_("TS","Segments"))
     if filled then fillersketch(xo2,gap_width,Ef,H,_("TS","Filler"));end
   else
     //segments near space/filler
     xo1=xo;
     Le1=[norm(PPh(:,8)-PPh(:,3));
          norm(PPl(:,8)-PPl(:,3))];
     Li1=[norm(PPh(:,7)-PPh(:,4));
          norm(PPl(:,7)-PPl(:,4))];
     xo2=xo1+max(Le1)+xshift
     if N>2*ngap then
       //regular segments
       Le2=[norm(PPh(:,2)-PPh(:,3));
            norm(PPl(:,2)-PPl(:,3))];
       Li2=[norm(PPh(:,1)-PPh(:,4));
            norm(PPl(:,1)-PPl(:,4))];
       xo3=xo2+max(Le2)+xshift 
     else
       xo3=xo2
     end
     xbnd=xo3
     if filled then  xbnd=xbnd+xshift+gap_width;end
     gca().data_bounds=[0    -E-yshift-2*wy;
                        xbnd  height+2*wy];
      SegSketch(xo1,Le1,Li1,E,H,_("TS","Segments around spacer"))
      if N>2*ngap then 
        SegSketch(xo2,Le2,Li2,E,H,_("TS","Regular Segments"));
      end;  
      if filled then 
        fillersketch(xo3,gap_width,Ef,H,_("TS","Filler"));
      end
   end
   s=_("TS","Outside view")
  r=xstringl(0,0,s,8,2)
  xstring(2*wx,(H-r(3))/2,s,-90)
  e=gce();e.font_style=8;e.font_size=2;
 
  s=_("TS","Top view")
  r=xstringl(0,0,s,8,2)
  xstring(2*wx,-yshift-E+(E-r(3))/2,s,-90)
  e=gce();e.font_style=8;e.font_size=2;
endfunction


function SegSketch(xo,Le,Li,E,H,tit) 
  dR=diff(Re)
  dx=max(Le)/2
  if dR<=0 then
    //vue de l'exterieur
    //1 face externe
    xe=[-Le(1);Le(1);Le(2);-Le(2);-Le(1)]/2+dx;
    ye=h1*[1;  1;    0;     0;    1];
    xpoly(xo+xe,ye);gce().thickness=2;
    coteh([xo+xe(4),ye(4)],-wy,Le(2),"mm") 
    cotev([xo+xe(3),ye(3)],wx,height,"mm")
    cotea([xo+xe(4) ye(4)],[%pi/2 %pi/2-MiterCutAngle],1.6*wx)
    //2 face interne
    xi=[-Li(1);Li(1);Li(2);-Li(2);-Li(1)]/2+dx;
    yi=[height ; height; h2;  h2;  height ];
    xpoly(xo+xi,yi);gce().line_style=3;gce().thickness=1;
    cotev([xo+xe(1),ye(1)],-wx,h2,"mm")
    
    //3 face sup
    xh=[-Le(1);Le(1);Li(1);-Li(1);-Le(1)]/2+dx;
    yh=[h1;h1;height;height;h1]
    xpoly(xo+xh,yh);gce().thickness=2;
    //4 face inf
    xl=[Le(2) -Le(2) ;Li(2) -Li(2)]/2+dx;
    yl=[0      0;     h2  h2];
    xsegs(xo+xl,yl);gce().line_style=3;gce().thickness=1;
    //vue de dessus
    //1 face sup
    yh=-E*[1;1;0;0;1];
    xh=[-Le(1);Le(1);Li(1);-Li(1);-Le(1)]/2+dx;
    xpoly(xo+xh,yh-yshift);gce().thickness=2;
    coteh([xo+xh(4);yh(4)-yshift],0.8*wy,Li(1),"mm")
    coteh([xo+xh(1);yh(1)-yshift],-0.8*wy,Le(1),"mm")    
    //2 face inf
    yl=-E*[1;1;0;0;1];
    xl=[-Le(2);Le(2);Li(2);-Li(2);-Le(2)]/2+dx;
    xpoly(xo+xl,yl-yshift);gce().thickness=2;
    coteh([xo+xl(4);yl(4)-yshift],1.6*wy,Li(2),"mm")
    cotev([xo+xl(2),yl(2)-yshift],wx,E,"mm")
    //cotea([xo+xl(1) yl(1)-yshift],[%pi/2 %pi/2-TiltCutAngle],min(E,wx*0.8))  
 
  else
    //vue de l'exterieur
    //1 face externe
    xe=[-Le(1);Le(1);Le(2);-Le(2);-Le(1)]/2+dx;
    ye=[height;  height;    h2;     h2;    height];
    xpoly(xo+xe,ye);gce().thickness=2;
    coteh([xo+xe(1),ye(1)],wy,Le(1),"mm") 
   
    cotev([xo+xe(2),ye(2)],wx,-height,"mm")
    cotea([xo+xe(1) ye(1)],[-%pi/2 -%pi/2+MiterCutAngle],1.6*wx)
    //2 face interne
    xi=[-Li(1);Li(1);Li(2);-Li(2);-Li(1)]/2+dx;
    yi=[h1 ; h1; 0;  0;  h1];
    xpoly(xo+xi,yi);gce().line_style=3;gce().thickness=1;
    cotev([xo+xe(4),ye(4)],-wx,-h2,"mm")
    //3 face sup
    xh=[Le(1) -Le(1) ;Li(1) -Li(1)]/2+dx;
    yh=[height  height;     h1  h1];
    xsegs(xo+xh,yh);gce().line_style=3;gce().thickness=1;
    
    //4 face inf
    xl=[-Le(2);Le(2);Li(2);-Li(2);-Le(2)]/2+dx;
    yl=[h2;h2;0;0;h2]
    xpoly(xo+xl,yl);gce().thickness=2;
    
    //vue de dessus
    //1 face inf
    yl=-E*[1;1;0;0;1];
    xl=[-Le(2);Le(2);Li(2);-Li(2);-Le(2)]/2+dx;
    xpoly(xo+xl,yl-yshift);gce().line_style=3;gce().thickness=1;
    coteh([xo+xl(4);yl(4)-yshift],0.8*wy,Li(2),"mm")
    coteh([xo+xl(1);yl(1)-yshift],-wy,Le(2),"mm")
    
    //2 face sup
    yh=-E*[1;1;0;0;1];
    xh=[-Le(1);Le(1);Li(1);-Li(1);-Le(1)]/2+dx;
    xpoly(xo+xh,yh-yshift);gce().thickness=2;
    coteh([xo+xh(4);yh(4)-yshift],1.6*wy,Li(1),"mm")
    
    cotev([xo+xh(2),yh(2)-yshift],wx,E,"mm")
    //cotea([xo+xh(1) yh(1)-yshift],[%pi/2 %pi/2-TiltCutAngle],min(E,wx*0.8))  
  end
  //axe
  xpoly(xo*[1;1]+dx,[-E-yshift-2*wy; height+2*wy]);gce().line_style=4;
  //titre
 
  r=xstringl(0,0,tit,8,2)
  
  xstring(xo+(max(Le)-r(3))/2,height+2*wy,tit)
  e=gce();e.font_style=8;e.font_size=2;
  
endfunction


function fillersketch(xo,gap_width,Ef,H,tit)
  dR=diff(Re)
  //vue de l'exterieur
  if dR<=0 then
    //face exterieure
     y=h1*[1;  1;    0;     0;    1];
     x=gap_width*[0;1;1;0;0];
     xpoly(xo+x,y);gce().thickness=2;
     //face sup
     y=[h1;h1;height;height;h1]
     xpoly(xo+x,y);gce().thickness=2;
     //face inf
     y=h2*[1;  1];
     x=gap_width*[0;1];
     xsegs(xo+x,y);gce().line_style=3;gce().thickness=1;
  else
      //face exterieure
     y=[height;  height;    0;     0;    height];
     x=gap_width*[0;1;1;0;0];
     xpoly(xo+x,y);gce().thickness=2;
     //face sup
     y=h1*[1;  1];
     x=gap_width*[0;1];
     xsegs(xo+x,y);gce().line_style=3;gce().thickness=1;
     //face inf
     y=h2*[1;  1];
     x=gap_width*[0;1];
     xsegs(xo+x,y);gce().thickness=2;
    
  end
 
  coteh([xo 0],-wy,gap_width,"mm")
  //vue de dessus
  x=[0;gap_width;gap_width;0;0];
  y=-Ef*[1;1;0;0;1]
  xpoly(xo+x,y-yshift);gce().thickness=2;
  cotev([xo+gap_width,min(y)-yshift],wx,Ef,"mm")
  //titre
  r=xstringl(0,0,tit,8,2)
  xstring(xo+(gap_width-r(3))/2,height+2*wy,tit)
  e=gce();e.font_style=8;e.font_size=2;
  
endfunction
