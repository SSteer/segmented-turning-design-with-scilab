function RE_Export2DRing(win)
//Filemenu callback for the "Export ring" sub menu    
//it generates a scaled PDF file 
  if type(win)==1 then
     mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  ud=mainH.user_data
  D=ud.D
  H=ud.H
  l=H.index.value
  RP=D.RingsProperties(l);
  path=uigetfile("*.pdf","",msprintf(_("TS","Give Ring#%d export file"),l));
  driver("PDF")
  xinit(path)
  px2cm=get(0,"screensize_cm")./get(0,"screensize_px");
  px2cm=px2cm(3);
  f=scf(max(winsid())+1);
  [X,Y,A,gap_index,C]=SegmentedRing2D(D,l,%f);
  [X,Y]=Rot2D(X,Y,RP.Rotation*2*%pi/RP.N)
  sz=max(X)/10
  f=gcf();f.axes_size=(2*sz/px2cm)*[1 1];
  ax=gca();
  ax.margins=zeros(1,4);
  ax.tight_limits="on";
  ax.data_bounds=sz*[-1 -1;1 1];
  ax.axes_visible="off";ax.isoview="on";
  xfpolys(X,Y,colors(C))
  xpoly(0,0,"marks");
  e=gce();e.mark_style=1;e.mark_size=3;
  xend()
  driver("Rec")
endfunction
