function [S,M]=ZigzagSegment3Dt(Ri_z,Re_z,z,A,zigzagprops)
//Re_z : vecteur des diametres exterieurs de la piece tournee aux cotes
//z
  A=A/2    
  m=max(2,ceil(50*A/%pi))
  alpha=linspace(-A,A,m)';
  Re_z=Re_z(:);Ri_z=Ri_z(:);z=z(:);
  Re=max(Re_z);Ri=min(Ri_z);
  
  mnz=min(z);z=z-mnz;
  l=2*Re*tan(A); h=max(z)
  p=Re-Ri*cos(A);
  zh=zigzagprops.zheight;
  thickness=zigzagprops.thickness;
  C=colors(zigzagprops.colors);

  thickness=[thickness($:-1:1) thickness(2:$)];
  C=[C($:-1:1) C(2:$)]
  //calcul de l'angle des couches
  dh=(h-zh)/2;
  e=sum(thickness(2:$-1));
  t=(e*zh+l*sqrt(zh^2+l^2-e^2))/(zh^2+l^2);
  az=atan((zh*t-e)/l,t);
  n=size(thickness,"*")
  h1=e/cos(az);
  
  z0=-thickness(1)/cos(az);
  z1=z0+l*tan(az)
  funcprot(0)
  projectedline=projectedline_L
  X1=[];Y1=[];Z1=[];Col1=[];
  Xm1=[];Ym1=[];Zm1=[];
  for k=1:n  
    h1=thickness(k)/cos(az);
    [Sl,Ml]=layer3D(z0,z1,h1,z,Re_z,Ri_z,alpha);
   
    X1=[X1 Sl.X];Y1=[Y1 Sl.Y];Z1=[Z1 Sl.Z];
    Col1=[Col1 ones(1,size(Sl.X,2))*C(k)];
    Xm1=[Xm1 Ml.X];Ym1=[Ym1 Ml.Y];Zm1=[Zm1 Ml.Z];
    z0=z0+h1;
    z1=z1+h1;
  end

  z0=-thickness(1)/cos(az);
  z1=z0+l*tan(az)
  projectedline=projectedline_R;
  funcprot(1)
  X2=[];Y2=[];Z2=[];Col2=[];
  Xm2=[];Ym2=[];Zm2=[];

  for k=1:n
    h1=thickness(k)/cos(az);
    [Sl,Ml]=layer3D(z0,z1,h1,z,Re_z,Ri_z,alpha);
    X2=[X2 Sl.X];Y2=[Y2 Sl.Y];Z2=[Z2 Sl.Z];
    Col2=[Col2 ones(1,size(Sl.X,2))*C(k)];
    Xm2=[Xm2 Ml.X];Ym2=[Ym2 Ml.Y];Zm2=[Zm2 Ml.Z];
    z0=z0+h1;
    z1=z1+h1;
  end
  [X2,Y2]=Rot2D(X2,Y2,-2*A)
  [Xm2,Ym2]=Rot2D(Xm2,Ym2,-2*A)
 
  S=struct("X",[X1 X2],"Y",[Y1 Y2],"Z",[Z1 Z2]+mnz,"C",[Col1 Col2])
  M=struct("X",[Xm1 Xm2],"Y",[Ym1 Ym2],"Z",[Zm1 Zm2]+mnz)
endfunction
function [x,y,z]=projectedline_L(z0,z1,z,R_z,alpha)
  Z=((z1-z0)/(2*A))*alpha+(z1+z0)/2
  R=interpln([z R_z]',Z)'
  x=R.*cos(alpha);
  y=R.*sin(-alpha);
  z=Z;
endfunction
function [x,y,z]=projectedline_R(z0,z1,z,R_z,alpha)
  Z=((z0-z1)/(2*A))*alpha+(z1+z0)/2
  R=interpln([z R_z]',Z)'
  x=R.*cos(alpha);
  y=R.*sin(-alpha);
  z=Z;
endfunction

function [S,M]=layer3D(z0,z1,h1,z,Re_z,Ri_z,alpha)
   H=max(z)
   if z0>=0 then
     [xle,yle,zle]=projectedline(z0,z1,z,Re_z,alpha);
     [xli,yli,zli]=projectedline(z0,z1,z,Ri_z,alpha);
   else
     [xle,yle,zle]=projectedline(-dh,-dh,z,Re_z,alpha);
     [xli,yli,zli]=projectedline(-dh,-dh,z,Ri_z,alpha);
   end
   if z1+h1<=H then
     [xhe,yhe,zhe]=projectedline(z0+h1,z1+h1,z,Re_z,alpha);
     [xhi,yhi,zhi]=projectedline(z0+h1,z1+h1,z,Ri_z,alpha);
   else
     [xhe,yhe,zhe]=projectedline(H-dh,H-dh,z,Re_z,alpha);
     [xhi,yhi,zhi]=projectedline(H-dh,H-dh,z,Ri_z,alpha);
   end
    X=[];Y=[];Z=[];
    Xm=[];Ym=[];Zm=[];
    
    j=1:m-1
   //face exterieure
   X=[X [xle(j) xle(j+1) xhe(j+1) xhe(j)]'];
   Y=[Y [yle(j) yle(j+1) yhe(j+1) yhe(j)]'];
   Z=[Z [zle(j) zle(j+1) zhe(j+1) zhe(j)]'];
   Xm=[Xm [xle;xhe($:-1:1)]];
   Ym=[Ym [yle;yhe($:-1:1)]];
   Zm=[Zm [zle;zhe($:-1:1)]];
   
   //face interieure
   X=[X [xli(j) xli(j+1) xhi(j+1) xhi(j)]'];
   Y=[Y [yli(j) yli(j+1) yhi(j+1) yhi(j)]'];
   Z=[Z [zli(j) zli(j+1) zhi(j+1) zhi(j)]'];
   Xm=[Xm [xli;xhi($:-1:1)]];
   Ym=[Ym [yli;yhi($:-1:1)]];
   Zm=[Zm [zli;zhi($:-1:1)]];
   
   //face basse
   if z0<=0 then
     X=[X [xle(j) xle(j+1) xli(j+1) xli(j)]'];
     Y=[Y [yle(j) yle(j+1) yli(j+1) yli(j)]'];
     Z=[Z [zle(j) zle(j+1) zli(j+1) zli(j)]'];
     Xm=[Xm [xle;xli($:-1:1)]];
     Ym=[Ym [yle;yli($:-1:1)]];
     Zm=[Zm [zle;zli($:-1:1)]];
   end
   
   //face haute
   if z1+h1>=H then
     X=[X [xhe(j) xhe(j+1) xhi(j+1) xhi(j)]'];
     Y=[Y [yhe(j) yhe(j+1) yhi(j+1) yhi(j)]'];
     Z=[Z [zhe(j) zhe(j+1) zhi(j+1) zhi(j)]'];
     Xm=[Xm [xhe;xhi($:-1:1)]];
     Ym=[Ym [yhe;yhi($:-1:1)]];
     Zm=[Zm [zhe;zhi($:-1:1)]];
   end
   Z=Z+dh;
   Zm=Zm+dh;
  
   S=struct("X",X,"Y",Y,"Z",Z,"C",[])
   M=struct("X",Xm,"Y",Ym,"Z",Zm)
endfunction
