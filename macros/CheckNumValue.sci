function [r,v]=CheckNumValue(bounds,h)
//Checks the answer given in Edit uicontrol
//It should be evaluated as a real scalar in [bounds(1), bounds(2)] interval
  
  if argn(2)==1 then h=gcbo;end
  if execstr("v=("+h.string+")","errcatch")<>0 then
    RE_SetError(h,%t,_("TS","Expression cannot be evaluated"))
    r=%f,v=[]
  elseif type(v)<>1|size(v,"*")<>1|~isreal(v) then
    RE_SetError(h,%t,_("TS","Real scalar expected"))
    r=%f
  else
    if argn(2)<1 then
      RE_SetError(h,%f)
      r=%t
    else
      r=bounds(1)<=v&v<=bounds(2)
      if r then
        RE_SetError(h,~r)//un hilite edit area
      else
        msg=msprintf(_("TS","Expected value must be in the interval [%g,  %g]"),bounds(1),bounds(2))
        RE_SetError(h,~r,msg)
      end
    end
  end
endfunction
