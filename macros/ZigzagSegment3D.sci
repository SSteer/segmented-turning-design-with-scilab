function [X,Y,Z,Col]=ZigzagSegment3D(Ri,Re,N,h,zigzagprops)
  N=2*N
  A=%pi/N;
  p=Re-Ri*cos(A);
  l=2*Re*tan(A);
  zh=zigzagprops.zheight
  thickness=zigzagprops.thickness
  C=zigzagprops.colors

  thickness=[thickness($:-1:1) thickness(2:$)];
  n=size(thickness,"*")
  C=[C($:-1:1)' C(2:$)']
  
  dh=(h-zh)/2
  
  e=sum(thickness(2:$-1))
  t=(e*zh+l*sqrt(zh^2+l^2-e^2))/(zh^2+l^2);
  az=atan((zh*t-e)/l,t);
  
  h1=e/cos(az);
  if thickness(1)<l*sin(az) then
    mprintf(_("TS","last thickness is to small <%g"),l*sin(az));
  end
  
  dl=p*tan(A)
  x=[];z=[];y=[];col=[];
  //left
  z0=-thickness(1)/cos(az);
  z1=z0+l*tan(az)
  h1=thickness(1)/cos(az);
  
  //couche basse
  //Faces visibles
  //face avant
  x=[x [ 0;   l;    l;    0   ]]; 
  z=[z [-dh; -dh; z1+h1; z0+h1]];
  y=[y [ 0;  0;    0;    0    ]];
  //face arriere
  x=[x [ dl;  l-dl;   l-dl;   dl  ]]; 
  z=[z [-dh; -dh;    z1+h1;  z0+h1]];
  y=[y [ p;    p;      p;      p  ]];
  //face inf
  x=[x [  0;    l;   l-dl;  dl ]]; 
  z=[z [ -dh; -dh;   -dh;  -dh ]];
  y=[y [  0;   0;     p;    p  ]];
  col=[col C(1)+emptystr(1,3)];
  if %f
    //Faces cachees
    //face gauche
    x=[x [ 0;    0;        dl;     dl ]]; 
    z=[z [-dh;   z0+h1;   z0+h1;  -dh ]];
    y=[y [ 0;    0;         p;      p ]];
    //face droite
    x=[x [   l;    l;   l-dl;  l-dl ]]; 
    z=[z [ -dh; z1+h1;  z1+h1; -dh  ]];
    y=[y [   0;    0;     p;    p   ]];
    //face sup
    x=[x [ 0;        l;   l-dl;   dl  ]]; 
    z=[z [ z0+h1;  z1+h1; z1+h1; z0+h1]];
    y=[y [ 0;        0;    p;    p    ]];
    col=[col C(1)+emptystr(1,3)];
  end
  z0=z0+h1;
  z1=z1+h1
  
  for k=2:n-1
    h1=thickness(k)/cos(az);
    //face avant
    x=[x [ 0;  l;    l;    0   ]]; 
    z=[z [ z0; z1; z1+h1; z0+h1]];
    y=[y [ 0;  0;    0;    0   ]];
    //face arriere
    x=[x [ dl; l-dl;  l-dl;   dl  ]]; 
    z=[z [ z0;  z1;   z1+h1; z0+h1]];
    y=[y [ p;    p;     p;     p  ]];
    col=[col C(k)+emptystr(1,2)];
    if %f
      //face gauche
      x=[x [ 0;    0;        dl;     dl ]]; 
      z=[z [ z0;   z0+h1;   z0+h1;   z0 ]];
      y=[y [ 0;    0;        p;      p  ]];
      //face droite
      x=[x [ l;    l;   l-dl;   l-dl  ]]; 
      z=[z [ z1; z1+h1; z1+h1;   z1   ]];
      y=[y [ 0;    0;    p;      p    ]];
      //face sup
      x=[x [ 0 ;      l;    l-dl;   dl  ]]; 
      z=[z [ z0+h1;  z1+h1; z1+h1; z0+h1]];
      y=[y [ 0;       0;      p;    p   ]];
      //face inf
      x=[x [ 0;    l;  l-dl;  dl ]]; 
      z=[z [ z0;   z1 z1;    z0; ]];
      y=[y [ 0;  0;    p;    p;   ]];
      col=[col C(k)+emptystr(1,4)];
    end
    z0=z0+h1;
    z1=z1+h1;
  end
  
  //couche haute
  h1=thickness(n)/cos(az);
  //face avant
  x=[x [ 0;  l;    l;    0  ]]; 
  z=[z [ z0; z1; h-dh;  h-dh]];
  y=[y [ 0;  0;    0;    0  ]];
  //face arriere
  x=[x [ dl; l-dl;  l-dl;   dl ]]; 
  z=[z [ z0;  z1;   h-dh;  h-dh]];
  y=[y [ p;    p;    p;    p   ]];
  //face sup
  x=[x [ 0;     l;   l-dl;  dl ]]; 
  z=[z [ h-dh; h-dh; h-dh; h-dh]];
  y=[y [ 0;     0;    p;    p  ]];
  col=[col C(n)+emptystr(1,3)];
  if %f
    //face gauche
    x=[x [ 0;    0;    dl;     dl]]; 
    z=[z [ z0;  h-dh;  h-dh;   z0]];
    y=[y [ 0;    0;    p;      p ]];
    //face droite
    x=[x [ l;    l;    l-dl;  l-dl]]; 
    z=[z [ z1;  h-dh;  h-dh;   z1 ]];
    y=[y [ 0;    0;     p;     p  ]];

    //face inf
    x=[x [ 0;    l; l-dl; dl ]]; 
    z=[z [ z0;  z1; z1;   z0 ]];
    y=[y [ 0;  0;    p;    p ]];
    col=[col C(n)+emptystr(1,3)];
  end
  z=z+dh
  
 
  [x,y]=Rot2D(x,y,-A,[l/2,p/2]);
  dx=(l-l*cos(A)-p*sin(A))/2
  x=x-dx
  X=x;Y=-y;Z=z;Col= col;
  
  //add the symetric segment
  X=[X 2*l*cos(A)-x];
  Y=[Y -y];
  Z=[Z z];
  Col=[Col col];
  //translate both segments to put them on the ring
  n=size(Y,2);
  X=X-l*cos(A)
  Y=Y+(-Y(4,n/2)+sqrt(Ri^2-X(4,n/2)^2));
endfunction

