function cotev(o,dx,l,unit)
  s=msprintf("%g\n",round(10*abs(l))/10)+unit;
  r=xstringl(0,0,s);

  xo=o(1)+dx
  yo=o(2)
  xpoly(o(1)+[0 1.5*dx],yo+[0 0])
  xpoly(o(1)+[0 1.5*dx],yo+[0 0]+l)
  
 
  xpoly(xo+[0 0],yo+[0 l])
  e=gce();e.line_style=3;e.polyline_style=4;
  xpoly(xo+[0 0],yo+[0.01 0])
  e=gce();e.line_style=3;e.polyline_style=4;
  if abs(l)>r(3) then
    if l>0 then
      xstring(xo,yo+(l-r(3))/2,s)
    else
       xstring(xo,yo+l+(abs(l)-r(3))/2,s)
    end
  else
    if l>0 then
      xstring(xo,yo+l,s)
    else
      xstring(xo,yo,s)
    end
  end
  e=gce();e.font_angle=-90;
endfunction
