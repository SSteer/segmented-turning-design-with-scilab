function PE_FileMenu(mainH)
//Creates the File menu for the RingEditor window
  w=string(mainH.figure_id);
  File=uimenu("parent", mainH, "label", _("File"),"Handle_Visible", "off"),
  Load = uimenu("parent"   , File, ..
                "label"    , _("TS","Load...")+" Ctrl+O",...
                "callback" ,list(4,"PE_Load("+w+")"))
 
  Save = uimenu("parent"   , File, ..
                "label"    , _("Save")+" Ctrl+S",...
                "callback" ,list(4,"PE_Save("+w+",%f)"))
  SaveAs = uimenu("parent"   , File, ..
                "label"    , _("TS","Save As")+'...',...
                "callback" ,list(4,"PE_Save("+w+")"))
  Close = uimenu("parent"   , File, ..
                 "label"    , _("Close"),...
                 "callback" ,list(4,"PE_Close("+w+")"))
  if mainH.user_data.caller<>[] then
    Apply = uimenu("parent"   , File, ..
                   "label"    , _("Apply"),...
                   "callback" ,list(4,"PE_Apply("+w+")")) 
  end
endfunction
