function Ok_defaults(win)
  if type(win)==1 then
    mainH=findobj("figure_id", win);
  else
    mainH=win;
  end
  Ok=gcbo;
  D=Ok.user_data.D;
  Path=Ok.user_data.Path;
  Handles=Ok.user_data.Handles // [Heights,Ewidth,Nsegs,Rot,Wood,Swood];
  Heights=evstr(Handles(1).string);
  if size(Heights,"*")==1 then
    Heights=Heights*ones(1,ceil(max(D.Pe.y)/Heights));
  end
  Ewidth=evstr(Handles(2).string);
  Nsegs=evstr(Handles(3).string);
  Rot=evstr(Handles(4).string);
  Wood=Handles(5).string(Handles(5).value);
  Swood=Handles(6).string(Handles(6).value);
  RP=rings_properties(size(Heights,"*"),Nsegs,Rot,Wood,Swood);

  D.H=Heights;
  D.Ewidth=Ewidth;
  D.RingsProperties=RP;
 
  D=Profile2Rays(D);
  ud=mainH.user_data;
  ud.D=D;
  ud.Path=Path;
  ud.Edited=%t;
  HDisplay=ud.H.Display;
  HDisplay.value=find(HDisplay.string==_("TS","2D view"));
  //ud.H.index.string="1"
  ud.H.index.string=string(1:size(D.H,"*"))'; ud.H.index.value=1
  mainH.user_data=ud;
  scf(mainH);
  RE_Update(mainH,D);
  mainH.immediate_drawing="on";
  mainH.figure_name=_("TS","Ring editor (%d) :")+fileparts(Path,"fname");
  delete(gcbo.parent.parent);
 endfunction
function rp=rings_properties(Nrings,Nsegs,Rot,Wood,Swood)
  //creates the defaut RingProperties data structure
  N=[]
  c=list();
  g=list();
  r=[];
  s=[];
  t=[];
  zz=list();
  st=list();
  for k=1:Nrings
    N=[N,Nsegs];
    g($+1)=struct("ngap",0,"gap",0,"type","full","filled",%f);
    c($+1)=Wood+emptystr(1,Nsegs);
    s($+1)=Swood;
    r($+1)=Rot;
    t($+1)="Standard";
    zz($+1)=struct("zheight",0,"thickness",[],"colors",[])
    st($+1)=struct("Re",[],"Ri",[])
  end
  rp=mlist(["rp","N","Rotation","Type","Colors","SpacerColor",...
            "GapProps" "ZigzagProps","StavesProps"],...
           N,r,t,c,s,g,zz,st);
endfunction
