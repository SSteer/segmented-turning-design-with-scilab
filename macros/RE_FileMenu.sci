function RE_FileMenu(mainH)
//Creates the File menu for the RingEditor window
  w=string(mainH.figure_id);
  File=uimenu("parent", mainH, "label", _("File"),"Handle_Visible", ...
              "off"),
  Import = uimenu("parent"   , File, ..
                "label"    , _("TS","Import profiles..."),...
                "callback" ,list(4,"RE_Import("+w+")"))
  Load = uimenu("parent"   , File, ..
                "label"    , _("TS","Load..."),...
                "callback" ,list(4,"RE_Load("+w+")"))
  Save = uimenu("parent"   , File, ..
                "label"    , _("Save"),...
                "callback" ,list(4,"RE_Save("+w+",%f)"))
  SaveAs = uimenu("parent"   , File, ..
                "label"    , _("TS","Save As"),...
                "callback" ,list(4,"RE_Save("+w+")"))
  Export = uimenu("parent"   , File, ..
                "label"    , _("TS","Export display"),...
                "callback" ,list(4,"RE_Export("+w+")"))
 
  Cuts=uimenu("parent"   , File, ..
                "label"    , _("TS","Export cuts"),...
                "callback" ,list(4,"RE_Cuts("+w+")"))
  
  Close = uimenu("parent"   , File, ..
                 "label"    , _("Close"),...
                 "callback" ,list(4,"RE_Close("+w+")"))
  
endfunction
