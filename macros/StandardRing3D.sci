function  [S,M]=StandardRing3D(N,Ri_z,Re_z,ze,RP)
  col=colors(RP.Colors(1));
  GapProps=RP.GapProps(1)
  gap=GapProps.gap
  ngap=GapProps.ngap
  filled=GapProps.filled
  if gap==0 then filled=%f;end
  typ=convstr(GapProps.type)

  Re_z=Re_z(:);
  Ri_z=Ri_z(:);
  ze=ze(:);
 
  [S,M]=Segment3Dt(N,Ri_z,Re_z,ze,GapProps);

  X=[];Y=[];Z=[];
  Xm=[];Ym=[];Zm=[];
  if ngap==0|gap==0|typ=="full"  then
    //no gap, all segments have the same shape
    [x,y,z]=(S(1).X,S(1).Y,S(1).Z);
    [xm,ym,zm]=(M(1).X,M(1).Y,M(1).Z);
   
    nf=size(x,2)//nombre de facette par segment
    A=%pi/N;
    a=0;
    for k=1:N
      [x,y]=Rot2D(x,y,a,[0,0]);
      [xm,ym]=Rot2D(xm,ym,a,[0,0]);
      X=[X x];Y=[Y y];Z=[Z z];
      Xm=[Xm xm];Ym=[Ym ym]; Zm=[Zm zm];
      a=2*A;
    end
    C=matrix(ones(nf,1)*col,1,-1);
    S=list(struct("X",X,"Y",Y,"Z",Z,"C",C))
    M=list(struct("X",Xm,"Y",Ym,"Z",Zm))
  else
    //rectangular spacer. 
    if ngap==N then
      //Segment3Dt returns 2 "segments" 
      //the first one is the segment 
      //the second one if the spacer
      [xseg,yseg,zseg]=(S(1).X,S(1).Y,S(1).Z)
      [xmseg,ymseg,zmseg]=(M(1).X,M(1).Y,M(1).Z)
      nf=size(xseg,2)
      if filled then
         Xspace=[];Yspace=[];Zspace=[];
         Xmspace=[];Ymspace=[];Zmspace=[];
        [xspace,yspace,zspace]=(S(2).X,S(2).Y,S(2).Z);
        [xmspace,ymspace,zmspace]=(M(2).X,M(2).Y,M(2).Z);
        nfs=size(xspace,2);
         Cspace=ones(1,ngap*nfs)*colors(RP.SpacerColor)
      end
      a=0
      for k=1:N
        [xseg,yseg]=Rot2D(xseg,yseg,a,[0,0]);
        [xmseg,ymseg]=Rot2D(xmseg,ymseg,a,[0,0]);
        X=[X xseg];Y=[Y yseg];Z=[Z zseg];
        Xm=[Xm xmseg];Ym=[Ym ymseg];Zm=[Zm zmseg];
        if filled then 
          [xspace,yspace]=Rot2D(xspace,yspace,a,[0,0]);
          [xmspace,ymspace]=Rot2D(xmspace,ymspace,a,[0,0]);
          Xspace=[Xspace xspace];Yspace=[Yspace yspace];Zspace=[Zspace zspace];
          Xmspace=[Xmspace xmspace]; Ymspace=[Ymspace ymspace];Zmspace=[Zmspace zmspace];
        end
        a=2*%pi/N ;
      end
      
      C=matrix(ones(nf,1)*col,1,-1);
      S=list(struct("X",X,"Y",Y,"Z",Z,"C",C))
      M=list(struct("X",Xm,"Y",Ym,"Z",Zm))
      if filled then
        S(2)=struct("X",Xspace,"Y",Yspace,"Z",Zspace,"C",Cspace),
        M(2)=struct("X",Xmspace,"Y",Ymspace,"Z",Zmspace),
      end

    else
      //in this case Segment3Dt returns 3 or 4 "segments"
      //the first one is the segment below the spacer
      //the second one if the segment above the spacer
      //the third one if the regular segment
      //the fourth ones is the spacer
      
       //upon spacer
      [xb,yb,zb]=(S(1).X,S(1).Y,S(1).Z);
      [xmb,ymb,zmb]=(M(1).X,M(1).Y,M(1).Z);
      nfb=size(xb,2);
      //above spacer
      [xa,ya,za]=(S(2).X,S(2).Y,S(2).Z);
      [xma,yma,zma]=(M(2).X,M(2).Y,M(2).Z);
      nfa=size(xa,2);
      //regular block
      [xr,yr,zr]=(S(3).X,S(3).Y,S(3).Z);
      [xmr,ymr,zmr]=(M(3).X,M(3).Y,M(3).Z);
      nfr=size(xr,2);
    
      //spacer
      if filled then
        Xspace=[];Yspace=[];Zspace=[];
        Xmspace=[];Ymspace=[];Zmspace=[];
        [xspace,yspace,zspace]=(S(4).X,S(4).Y,S(4).Z);
        [xmspace,ymspace,zmspace]=(M(4).X,M(4).Y,M(4).Z);
        nfs=size(xspace,2); 
      else
        nfs=0
      end
      
      
      a=3*%pi/N;b=0;
      scount=0
      for i=1:ngap
        [xb,yb]=Rot2D(xb,yb,b)
        [xmb,ymb]=Rot2D(xmb,ymb,b)
        X=[X xb];Y=[Y yb];Z=[Z,zb];
        Xm=[Xm xmb];Ym=[Ym ymb];Zm=[Zm,zmb];
        scount=scount+1
        C=[C ones(1,nfb)*col(scount)]
        if filled then 
          [xspace,yspace]=Rot2D(xspace,yspace,b)
          [xmspace,ymspace]=Rot2D(xmspace,ymspace,b)
          Xspace=[Xspace xspace];
          Yspace=[Yspace yspace];
          Zspace=[Zspace,zspace];
          Xmspace=[Xmspace xmspace];
          Ymspace=[Ymspace ymspace];
          Zmspace=[Zmspace,zmspace];
        end
        [xa,ya]=Rot2D(xa,ya,b)
        [xma,yma]=Rot2D(xma,yma,b)
        scount=scount+1
        C=[C ones(1,nfa)*col(scount)]
        X=[X xa];Y=[Y ya];Z=[Z,za];
        Xm=[Xm xma];Ym=[Ym yma];Zm=[Zm,zma];
        for k=1:(N-2*ngap)/ngap     
          [xr,yr]=Rot2D(xr,yr,a)
          [xmr,ymr]=Rot2D(xmr,ymr,a)
          scount=scount+1
          C=[C ones(1,nfr)*col(scount)]
          X=[X xr];Y=[Y yr];Z=[Z,zr];
          Xm=[Xm xmr];Ym=[Ym ymr];Zm=[Zm,zmr];
          a=2*%pi/N ;
        end
        b=2*%pi/ngap
        a=6*%pi/N ;
      end
    end
    A=%pi/N
    [X,Y]=Rot2D(X,Y,A);
    [Xm,Ym]=Rot2D(Xm,Ym,A);
    S=list(struct("X",X,"Y",Y,"Z",Z,"C",C))
    M=list(struct("X",Xm,"Y",Ym,"Z",Zm))
    if filled then
      Cspace=ones(1,size(Xspace,2))*colors(RP.SpacerColor);
      [Xspace,Yspace]=Rot2D(Xspace,Yspace,A);
      [Xmspace,Ymspace]=Rot2D(Xmspace,Ymspace,A);

      S(2)=struct("X",Xspace,"Y",Yspace,"Z",Zspace,"C",Cspace),
      M(2)=struct("X",Xmspace,"Y",Ymspace,"Z",Zmspace),
    end
  end
endfunction

