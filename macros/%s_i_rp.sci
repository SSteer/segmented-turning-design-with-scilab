function dest=%s_i_rp(i,src,dest)
//when src=[]
  if src<>[] then error("This should not happen");end
  fn=fieldnames(dest)'
  for f=fn;
    if or(f==["GapProps" "Colors" "StavesProps" "ZigzagProps"]) then 
      df=dest(f)
      df(i)=null()
      dest(f)=df
    else
      dest(f)(i)=[];
    end
  end
endfunction
