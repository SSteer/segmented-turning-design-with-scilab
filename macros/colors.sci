function c=colors(C)
  //converts an array of hexadecimal coded RGB colors into colors index in
  //the current color_map
  C=C(:);
  
  nc=size(C,"*");
  c=zeros(1,nc);
  khex=find(part(C,1)=="#");
  kn=1:nc;
  if khex<>[] then
    rgb=255*hex2rgb(C(khex));
    for k=1:size(rgb,1)
      c(khex(k))=color(rgb(k,1),rgb(k,2),rgb(k,3));
    end
    kn(khex)=[];
  end
  for k=kn
    c(k)=color(C(k));
  end
endfunction

