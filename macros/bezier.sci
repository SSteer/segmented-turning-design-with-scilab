function zi=bezier(z,NPOI,modus);
// BEZIER	Cubic Bezier splines
//
//		ZI=BEZIER(Z,NPOI,MODUS)
//
//		Z is a vector, which contains the weighing points.
//		NPOI is the number of interpolating values.
//		MODUS is a string which specifies, if curves are
//		closed (MODUS='c[lose]') or
//		open (MODUS='o[pen]').
//
//		examples:
//			zout=bezier(z);
//			NPOI is defined as 40, modus is defined as "close"
//
//			zout=bezier(z,NPOI);
//			modus is defined as "closed".			
  [%nargout,%nargin]=argn(0);
  if %nargin==2 then,
    if type(NPOI)==10,
      modus=NPOI;
      NPOI=40;
    else
      modus='close';
    end
  end

  modus=convstr(part(modus,1),"l");
  z=matrix(z,1,-1);
  if modus=='c' then,
    z=[z,z(1:3)];
  end
  LZ=length(z);

  // Bestimmung der Bezier-Punkte
  zr=[0,z(1:$-1)];
  zl=[z(2:$),0];
  b0=z(1);
  b1=(2*zr + z)/3;		b1=b1(2:LZ);		// b1, b4, b7, ...
  b3=(zr + 4*z + zl)/6; 	b3=b3(2:(LZ-1));	// b3, b6, b9, ...
  b2=(z + 2*zl)/3;		b2=b2(1:(LZ-1));	// b2, b5, b8, ...

  v = linspace(0,1,NPOI);
  v = v(1:NPOI-1);
  vc = 1 - v;
  zi = [];

  // connection from point 0 to point 1
  // in case of open curves
  // ---------------------------------
  if modus~='c',
    zi=b0*(vc.^3) + 3*b1(1)*v.*(vc.^2) + 3*b2(1)*vc.*(v.^2) + b3(1)*(v.^3);
  end
 
  for k=2:(LZ-2)
    zout=b3(k-1).*(vc.^3) + 3*b1(k).*v.*(vc.^2) + 3*b2(k).*vc.*(v.^2) + b3(k).*(v.^3);
    zi=[zi,zout];
  end
  zi=[zi z($)]
endfunction;
