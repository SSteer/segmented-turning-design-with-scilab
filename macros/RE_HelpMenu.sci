function RE_HelpMenu(mainH)
//Creates the File menu for the RingEditor window
  w=string(mainH.figure_id);
  Help=uimenu("parent", mainH, "label", "&"+_("Help"),"Handle_Visible", ...
              "off",...
              "callback" ,list(4,"help RingEditor"))
endfunction
