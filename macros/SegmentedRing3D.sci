function [S,M]=SegmentedRing3D(D,l,view)
//computes and draw, if requested, the 3D representation of the different
//segments (segment, spacer,) for the given ring index l according to the
  
//http://www.segmentedturning.com/RingTypes.htm
  if argn(2)<3 then
    view=%f
  elseif and(view<>[%t %f]) then
     error(msprintf(_("%s: Wrong type for argument #%d: Boolean expected.\n"),...
                    "SegmentedRing3D",3))
  end
  RP=D.RingsProperties(l);
  select RP.Type
  case "Standard"
    [X,Y,Z,C]=SegmentedStandardRing3D(D,l)
  case "Zigzag"
    [X,Y,Z,C]=SegmentedZigzagRing3D(D,l)
  case "Staves"
    [X,Y,Z,C]=SegmentedStavesRing3D(D,l)
  end
  S=list(struct("X",X,"Y",Y,"Z",Z,"C",C))
  M=list(struct("X",[],"Y",[],"Z",[]))
  if view & X<>[]then
    ax=gca();
    delete(ax.children)
    plot3d(X,Y,list(Z,C))
    ax.data_bounds=[min(X) min(Y) min(Z);max(X) max(Y) max(Z)];
    ax.isoview="on";
    gce().hiddencolor=0;
    xtitle(msprintf(_("TS","Ring number %d"),l))
  end
endfunction
