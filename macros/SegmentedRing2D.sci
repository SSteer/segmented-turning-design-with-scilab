function [X,Y,A,gap_index,C]=SegmentedRing2D(D,l,view)
//The 2D representation of the different segments types (segment, spacer,)
//for the given ring index l according to the ring properties
  if argn(2)<3 then
    view=%f
  elseif and(view<>[%t %f]) then
    error(msprintf(_("%s: Wrong type for argument #%d: Boolean expected.\n"),"Ring3d",3))
  end
  RP=D.RingsProperties(l);
  N=RP.N;
  A=%pi/N;
  if or(RP.Type==["Standard" "Staves"]) then
    if RP.Type=="Standard" then
      Re=D.Re(l);Ri=D.Ri(l);
      Ree=Re+D.Ewidth;
      Rie=max(0,Ri-D.Ewidth);
    else
      StavesProps=RP.StavesProps(1)
      Re=max(StavesProps.Re);//bottom and top Radii
      Ri=max(StavesProps.Ri);
      Ree=Re+D.Ewidth;
      Rie=max(0,Ri-D.Ewidth);
    end
    GapProps=RP.GapProps(1);
    gap_width=GapProps.gap
    ngap=GapProps.ngap
    filled=GapProps.filled
 
    typ=convstr(GapProps.type)
    col=RP.Colors(1);
   
    [x,y]=Segment2D(Rie,Ree,N,GapProps);
    gap_index=[]
    X=[];Y=[];C=[];
    if ngap==0|gap_width==0|typ=="full"  then
      //no gap, all segments have the same shape
      a=0;
      for k=1:N
        [x,y]=Rot2D(x,y,a,[0,0]);
        X=[X x];
        Y=[Y y];
        a=2*A;
      end
    else
      //rectangular spacer   
      //here the segments keeps their miter angle equal to %pi/N
      if ngap==N then
        //all segments are the same
        X=[];Y=[];
        a=0
        for k=1:N
          [x,y]=Rot2D(x,y,a,[0,0]);
          if filled then gap_index=[gap_index size(X,2)+2];end
          X=[X,x];
          Y=[Y,y];
          a=2*A;
        end
      else
        //the are 3 different segment shapes a
        // those that immediately  precede and  follow the space/spacer 
        //  and the other ones.
        // the previous call to Segment2D generates  those that immediately
        // precede and  follow the space/space and the spacer shapes
        // Segment2D is called here to set the other segments if any
        [xr,yr]=Segment2D(Rie,Ree,N);
        a=3*%pi/N;b=0;
        
        for l=1:ngap
          [x,y]=Rot2D(x,y,b)
          if filled then gap_index=[gap_index size(X,2)+2];end
          X=[X x];Y=[Y y]
          
          for k=1:(N-2*ngap)/ngap     
            [xr,yr]=Rot2D(xr,yr,a)
            X=[X xr];Y=[Y yr]
            a=2*%pi/N ;
          end
          
          b=2*%pi/ngap
          a=6*%pi/N ;
        end
      end
    end
    nf=size(X,2);
    ind=1:nf;
    ind(gap_index)=[];
    C=emptystr(1,nf);

    C(ind)=col;
    if gap_index<>[] then
      C(gap_index)=RP.SpacerColor
    end
  else //zigzag
    Re=D.Re(l);Ri=D.Ri(l);
    Ree=Re+D.Ewidth;
    Rie=max(0,Ri-D.Ewidth);
    ZigzagProps=RP.ZigzagProps(1);
    col=ZigzagProps.colors
    [x,y]=Segment2D(Rie,Ree,N)
     X=[];Y=[];
     a=0;
     for k=1:N
       [x,y]=Rot2D(x,y,a,[0,0]);
       X=[X x];
       Y=[Y y];
       a=2*A;
     end
     C=emptystr(1,N)+ZigzagProps.colors($)
     ind=1:N;
     gap_index=[]
  end
  
 
  if view then
    ax=gca()
    delete(ax.children)
    ax.isoview='on';
    ax.margins=0.05*ones(1,4);
    ax.axes_visible=["on" "on"];
    [X,Y]=Rot2D(X,Y,RP.Rotation*2*%pi/N)
    xfpolys(X,Y,colors(C));gce().children.thickness=2;
    ax.data_bounds=[min(X) min(Y) ;max(X) max(Y)];
    t=linspace(0,2*%pi,50);
    plot(Ri*cos(t),Ri*sin(t),'--r');
    
    plot(Re*cos(t),Re*sin(t),'--r');
   
    if or(RP.Type==["Standard" "Zigzag"]) then
      if Ri>0 then
        xpoly([0 Ri],[0 0]);e=gce();e.polyline_style=4;arrow_size_factor = 3;
        xstring(Ri/4,0,msprintf("%g mm",round(Ri)))
      end
      t=0.5;
      xpoly([0 Re*cos(t)],[0 Re*sin(t)]);
      e=gce();e.polyline_style=4;arrow_size_factor = 3;
      xstring(Re/4*cos(t),Re/4*sin(t),...
              msprintf("%g mm",round(Re)),-t*180/%pi);
      e=gce(); 
    else
      xpoly([0 Ri],[0 0]);
      e=gce();e.polyline_style=4;arrow_size_factor = 3;
      xstring(Ri/4,0,msprintf("%.1f -> %.1f mm",StavesProps.Ri(1),StavesProps.Ri(2)))
      t=0.5
      xpoly([0 Re*cos(t)],[0 Re*sin(t)]);e=gce();e.polyline_style=4;arrow_size_factor = 3;
      xstring(Re/4*cos(t),Re/4*sin(t),...
              msprintf("%.1f -> %.1f mm",StavesProps.Re(1),StavesProps.Re(2)),-t*180/%pi);
      e=gce(); 
      
    end
 
   //compute segments' barycenters
    Gx=mean(X(1:4,ind),1);
    Gy=mean(Y(1:4,ind),1);
    //Display segments' indices
    for k=1:size(Gx,"*")
      rect=xstringl(0,0,string(k));
      w=rect(3)/2;
      h=rect(4)/2;
      xnumb(Gx(k)-w,Gy(k)-h,k);
    end
  
    xtitle(msprintf(_("TS","Ring number %d"),l))
  end
endfunction
