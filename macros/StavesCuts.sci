function  T=StavesCuts(lr,H,RP)
  T=[]
  N=RP.N;
 
  A=%pi/N; 
  [Col, km, kn, nc]=unique(RP.Colors(1))
  
  [c,cn]=wood_colormap();c=rgb2hex(c)
  [w,k]=intersect(c,Col)
  Col=cn(k)
  Cols=RP.SpacerColor
  Cols=cn(find(Cols==c,1))
  GapProps=RP.GapProps(1);
  StavesProps=RP.StavesProps(1);
 
  typ=GapProps.type
  gap_width=GapProps.gap
  ngap=GapProps.ngap
  filled=GapProps.filled
  
  Re=StavesProps.Re+D.Ewidth;
  Ri=max(0,StavesProps.Ri-D.Ewidth/cos(A));
  //astuce pour assurer que les parois de la douve sont paralleles
  dR=max(Re-Ri)
  Ri=Re-dR
   
  [SplayAngle,MiterCutAngle,TiltCutAngle]=StaveAngles(A,diff(Re),H);

  [PPh,AAh]=SegmentPoints(A,Ri(2),Re(2),gap_width);
  [PPl,AAl]=SegmentPoints(A,Ri(1),Re(1),gap_width);
  Ef=norm(PPh(:,6)-PPh(:,5))*cos(SplayAngle);
  E=norm(AAh(:,3)-AAh(:,2))*cos(SplayAngle);
  h1=H/cos(SplayAngle);h2=E*tan(SplayAngle);
  height=h1+h2;
  
  if diff(Re)<0 then SplayAngle=-SplayAngle;end
  if ngap==0|gap_width==0|typ=="full"  then
    Le=max(norm(PPh(:,2)-PPh(:,3)) ,norm(PPl(:,2)-PPl(:,3)));
    for kc=1:size(nc,"*")
      T=[T;Out(lr,Col(kc),nc(kc),max(Re),Le,TiltCutAngle,MiterCutAngle,SplayAngle,height,E)];
    end
  else
    //rectangular spacer   
    if ngap==N then
      //all segments are the same 
      Le=max(norm(PPh(:,8)-PPh(:,6)),norm(PPl(:,8)-PPl(:,6)));
      for kc=1:size(nc,"*")
        T=[T;Out(lr,Col(kc),nc(kc),max(Re),Le,TiltCutAngle,MiterCutAngle,SplayAngle,height,E)];
      end
      if filled then
        T=[T;Out(lr,Cols,ngap,max(Re),gap_width,0,0,SplayAngle,height,Ef)];
      end
    else
      //the are 2 different segment shapes a
      // those that immediately  precede and  follow the space/spacer 
      //  and the other ones.
      
      Le=max(norm(PPh(:,8)-PPh(:,3)),norm(PPl(:,8)-PPl(:,3)));
      nsr=(N-2*ngap)/ngap //number of regular segments between 2 gaps
      ind=1:2;for k=2:ngap, ind=[ind ind($)+nsr+(1:2)];end
      for kc=1:size(nc,"*")
        T=[T;Out(lr,Col(kc),nc(kc),max(Re),Le,TiltCutAngle,MiterCutAngle,SplayAngle,height,E)];
      end
      //Segments reguliers
      if N>2*ngap then
        Le=max(norm(PPh(:,2)-PPh(:,3)),norm(PPl(:,2)-PPl(:,3)));
        indr=1:N;indr(ind)=[];
        [Col, km, kn, nc]=unique(RP.Colors(indr))
        for kc=1:size(nc,"*")
          T=[T;Out(lr,Col(kc),nc(kc),max(Re),Le,TiltCutAngle,MiterCutAngle,SplayAngle,height,E)]; 
        end
      end
      if filled then
        T=[T;Out(lr,Cols,ngap,max(Re),gap_width,0,0,SplayAngle,height,Ef)]
      end
    end
  end
endfunction

function T=Out(l,col,n,Re,Le,TiltCutAngle,MiterCutAngle,SplayAngle,H,E)
  d=100;
  t=2;
  bl=ceil(n.*(Le+t)  );
  D=round(2*d*Re)/d;
  Le=round(d*Le)/d;
  //Li=round(d*Li)/d;
  E=ceil(d*E)/d;
  H=ceil(d*H)/d;
  TiltCutAngle=round(d*TiltCutAngle*180/%pi)/d;
  MiterCutAngle=round(d*MiterCutAngle*180/%pi)/d;
  SplayAngle=round(d*SplayAngle*180/%pi)/d;
  if xls_export then
    T={l,col,D,n,Le,TiltCutAngle,MiterCutAngle,SplayAngle,H,E,bl};
  else
    T=strsubst(msprintf("%i;%s;%g;%i;%g;%g;%g;%g;%g;%g;%g\n",...
                        l,col,D,n,Le,TiltCutAngle,MiterCutAngle,SplayAngle,H,E,bl),".",",");  
  end
endfunction
