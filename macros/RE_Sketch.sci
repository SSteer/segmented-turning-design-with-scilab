function RE_Sketch(D,l)
  ax=gca();
  drawlater()
  ax.view="2d";
  ax.axes_visible=["off" "off"];
 
  ax.margins=0.01*ones(1,4);
  ax.isoview="on"
 
  RP=D.RingsProperties(l);
 
  select RP.Type
  case "Zigzag" then 
     ZZ_Design(D,l)
  case "Standard" then
    StandardSketche(D,l)
  case "Staves" then
    StaveSketche(D,l)
  end
  drawnow()
endfunction
