function rgb=hex2rgb(h)
//Converts an array of hex coded colors into an [R G B] array
  rgb=[hex2dec(part(h,2:3)) hex2dec(part(h,4:5)) hex2dec(part(h,6:7))]/255;
endfunction
