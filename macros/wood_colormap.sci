function [C,Cn]=wood_colormap()
//returns  in C the RGB definition of the wood colormap and in Cn the
//name of the associated wood
  C=[
      199 143 68  //aulne
      228 214 150 //Bouleau
      207 151 102 //Cedre
      236 218 118 //Chataigner
      210 185 80  //Chene
      40  45  48  //Ebene
      247 225 124 //Erable
      255 220 110 //frene
      240 203 115 //hetre
      216 137 71  //If
      161 104 35  //ipe
      224 129 21  //merisier
      188 113 30  //moabi
      123 73  14  //Noyer
      206 187 155 //Olivier
      211 66  49  //Padouk
      253 224 122 //peuplier
      178 135 127  //Pommier
      168 100 36  //Prunier
      220 220 220 //Sycomore
      228 208 121 //tilleul
    ];
  
  C=C/256;
 
  Cn=[_("TS","Alder");_("TS","Birsh");_("TS","Cedar");_("TS","Chestnut");
      _("TS","Oak");_("TS","Ebony");_("TS","Maple");
      _("TS","Ash");_("TS","Beech");_("TS","Yew");
      _("TS","Ipe");_("TS","Cherry");_("TS","Moabi");
      _("TS","Walnut");_("TS","Olivier");_("TS","Padouk");
      _("TS","Poplar");_("TS","Apple tree");
      _("TS","Plum tree");_("TS","Sycamore");_("TS","Liden")];

  [Cn,k]=gsort(Cn,'lr',"i")
  C=C(k,:)
endfunction
