function RE_Defaults(win,D,Path)
  if type(win)==1 then
    mainH=findobj("figure_id", win)
  else
    mainH=win
  end
  
  pheight=max(D.Pe.y)
  
  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 5;      // Vertical margin between each elements
  button_w     = 140;
  button_h     = 30;
  label_h      = 25;
  label_w      = 200;
  bg=color("lightgray");
  defaultfont  = "arial"; 
  
  guiHandle = figure("menubar", "none", ...
                     "toolbar","none", ...
                     "infobar_visible", "off",  ...
                     "axes_size",[280,365],...
                     "figure_name",_("TS","Default properties")+" (%d)",...
                     "visible","off",...
                     "event_handler","",...
                     "event_handler_enable", "off",...
                     "tag","Defaults");
  
  frame_border=createBorder("titled",createBorder("etched"),...
                            _("TS","Default properties"),...
                            "left","top",createBorderFont("Sans Serif",12,"bold"),"blue")
  list_h=5*label_h*1.2
  wf1=5*margin_x+label_w+50;hf1=17*margin_y+10.5*label_h
  
  xf1=margin_x;yf1=365-hf1;
  f1=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [xf1 yf1 wf1 hf1],....
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , 0.85*ones(1,3),...
      "border"              , frame_border);
  //Heights of the rings
  x=margin_x;y=hf1-4*margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Heights of rings mm"));
  y=y-margin_y-label_h*1.5
  Heights=CreateEdit(f1,x,y,label_w+50+margin_x,label_h*1.5,..
                     "20","Heights","CheckHeights("+string(pheight)+")");
  //Extra width
  x=margin_x; y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Fudge factor mm"));
  x=x+margin_x+label_w;
  Ewidth=CreateEdit(f1,x,y,50,label_h,..
                     "3","Ewidth","CheckNumValue([0,%inf])");

  //Nsegs
  x=margin_x;y=y-margin_y-label_h*1.5;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Number of segments per ring"));
  x=x+margin_x+label_w;
  Nsegs=CreateEdit(f1,x,y,50,label_h,"12","Nsegs","CheckNumValue([1 %inf])");
  //Rotation angle
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Rotation angle"));
  x=x+margin_x+label_w;
  Rot=CreateEdit(f1,x,y,50,label_h,"0.5","Rot","CheckNumValue([-1 1])");

  //Segments wood
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Segments wood"));
  x=x+margin_x+30;
  y=y-margin_y-label_h
  [C,Cn]=wood_colormap()
  hc=rgb2hex(C);
  choices=[hc,Cn,hc]
  Wood=CreatePopup(f1,x,y,0.9*label_w,label_h,choices,"wood","");

  //Spacer wood
  x=margin_x;
  y=y-margin_y-label_h;
  CreateLabel(f1,x,y,label_w,label_h,_("TS","Spacer wood"));
  x=x+margin_x+30
  y=y-margin_y-label_h
  [C,Cn]=wood_colormap()
  hc=rgb2hex(C);
  choices=[hc,Cn,hc]
  Swood=CreatePopup(f1,x,y,0.9*label_w,label_h,choices,"swood","");
  
  //Buttons
  w=string(guiHandle.figure_id)
  win=string(mainH.figure_id)
  x=margin_x;
  y=y-margin_y-label_h;
  Ok=CreateButton(f1,x,y,0.6*label_w,label_h,_("TS","Apply"),"Ok","Ok_defaults("+win+")")
  Handles=[Heights,Ewidth,Nsegs,Rot,Wood,Swood];
  Ok.user_data=struct("Handles",Handles,"mainH",mainH,"D",D,"Path",Path);
  x=x+margin_x+0.6*label_w
  CreateButton(f1,x,y,0.6*label_w,label_h,_("TS","Cancel"),"Cancel","delete(findobj(""figure_id"","+w+"))")

  guiHandle.visible="on";
endfunction
