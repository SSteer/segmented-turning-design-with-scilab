function  T=StandardCuts(lr,Re,Ri,H,RP)
  T=[];
  N=RP.N;
  [Col, km, kn, nc]=unique(RP.Colors(1));
  [c,cn]=wood_colormap();c=rgb2hex(c);
  [w,k]=intersect(c,Col);
  Col=cn(k);
  Cols=RP.SpacerColor;
  Cols=cn(find(Cols==c,1));
  GapProps=RP.GapProps(1);
  typ=GapProps.type;
  gap=GapProps.gap;
  ngap=GapProps.ngap;
  filled=GapProps.filled;
  A=%pi/N;  
  [PP,AA]=SegmentPoints(A,Ri,Re,gap);
  gap_width=gap;
  if ngap==0|gap==0|typ=="full"  then
    E=norm(AA(:,3)-AA(:,2));
    Le=norm(PP(:,2)-PP(:,3));
    Li=norm(PP(:,1)-PP(:,4));
    for kc=1:size(nc,"*")
      T=[T;Out(lr,Col(kc),nc(kc),Re,Le,Li,A,H,E)];
    end
  else //rectangular spacer   
    if ngap==N then
      //all segments are the same 
      E=norm(AA(:,3)-AA(:,2));
      Le=norm(PP(:,8)-PP(:,6));
      Li=norm(PP(:,7)-PP(:,5));
      for kc=1:size(nc,"*")
        T=[T;Out(lr,Col(kc),nc(kc),Re,Le,Li,A,H,E)];
      end
      if filled then
        Ef=norm(PP(:,6)-PP(:,5));
        T=[T;Out(lr,Cols,ngap,Re,gap_width,gap_width,0,H,Ef)];
      end
    else
      //segments around space/spacer
      Le=norm(PP(:,8)-PP(:,3));
      Li=norm(PP(:,7)-PP(:,4));
      E=norm(AA(:,3)-AA(:,2));
      nsr=(N-2*ngap)/ngap //number of regular segments between 2 gaps
      ind=1:2;for k=2:ngap, ind=[ind ind($)+nsr+(1:2)];end
      [Col1, km, kn, nc]= unique(RP.Colors(1)(ind));
      [w,k]=intersect(c,Col1);
      Col1=cn(k);
      for kc=1:size(nc,"*")
        T=[T;Out(lr,Col1(kc),Re,nc(kc),Le,Li,A,H,E)]; //revoir les couleurs
      end
      //Regular segments
      if N>2*ngap then
        E=norm(AA(:,3)-AA(:,2));
        Le=norm(PP(:,2)-PP(:,3));
        Li=norm(PP(:,1)-PP(:,4));
       
        indr=1:N;indr(ind)=[];
        [Colr, km, kn, nc]=unique(RP.Colors(1)(indr))
        [w,k]=intersect(c,Colr);
        Colr=cn(k);
        for kc=1:size(nc,"*")
          T=[T;Out(lr,Colr(kc),nc(kc),Re,Le,Li,A,H,E)]; 
        end
      end
      if filled then 
        Ef=norm(PP(:,6)-PP(:,5));
        T=[T;Out(lr,Cols,ngap,Re,gap_width,gap_width,0,H,Ef)];
      end
    end
  end
endfunction

function T=Out(l,col,n,Re,Le,Li,A,H,E)
  d=100;
  t=2;
  bl=ceil(n.*(Le+Li+2*t)/2);
  D=round(2*d*Re)/d;
  Fp=round((Le*cos(A)*d))/d;
  Le=round(d*Le)/d;
  Li=round(d*Li)/d;
  E=ceil(d*E)/d;
 
  H=ceil(d*H)/d;
  A=round(d*A*180/%pi)/d;
  if xls_export then
    T={l,col,D,n,Le,A,Fp,H,E,bl};
  else
    T=strsubst(msprintf("%i;%s;%g;%i;%g;%g;%g;%g;%g\n",...
                        l,col,D,n,Le,A,Fp,H,E,bl),".",",");
  end
endfunction
