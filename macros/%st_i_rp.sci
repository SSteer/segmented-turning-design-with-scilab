function to=%st_i_rp(i,from,to)
  fn=fieldnames(to)';
  for f=fn
    to(f)(i)=from(f)
  end
endfunction
