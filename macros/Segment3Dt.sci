function [S,M]=Segment3Dt(N,Ri_z,Re_z,z,GapProps)
//Computes the 3D coordinates of the different turned segments according
//the the ring properties
 //N number of segments in the ring, 
//z is the discretization of the elevation of the segment
//Ri_z is the associated abscissae of the interior of the segment
//Re_z is the associated abscissae of the exterior of the segment
//GapProps: ring properties
//S3D is a list which contains 
//  - the 3D representation of the segment above space/spacer
//  - the 3D spacer representation if the space is filled
//  - the 3D representation of the segment upon space/spacer  

  Re=max(Re_z);
  Ri=min(Ri_z);
  Re_z=Re_z(:);
  Ri_z=Ri_z(:);
  z=z(:);
 
  if argn(2)<5 then
    opt="full"
    filled=0
    n=0;gap_width=0;
  else
    opt=convstr(GapProps.type)
    filled=GapProps.filled
    ngap=GapProps.ngap
    gap_width=GapProps.gap
  end

  X=[];Y=[];Z=[];
  if ngap==0|gap_width==0|typ=="full"  then
    A=%pi/N;
    [S,M]=TrapezoidalPiece3D(Ri_z,Re_z,z,A*ones(Ri_z),A*ones(Re_z))
    //the right and left facets are hidden so we can remove them
    S.X(:,5:6)=[];S.Y(:,5:6)=[];S.Z(:,5:6)=[];
    M.X(:,5:6)=[];M.Y(:,5:6)=[];M.Z(:,5:6)=[];
   

  else   //Rectangular spacer
    A=%pi/N;
    if ngap==N then
      Ae=atan(gap_width./(2*Re_z));
      Ai=atan(gap_width./(2*Ri_z));
      [S,M]=TrapezoidalPiece3D(Ri_z,Re_z,z,A-Ai,A-Ae);
      [S.X,S.Y]=Rot2D(S.X,S.Y,A);
      [M.X,M.Y]=Rot2D(M.X,M.Y,A);
      if filled then
        //the right and left facets are hidden so we can remove them
        S.X(:,5:6)=[];S.Y(:,5:6)=[];S.Z(:,5:6)=[];
        M.X(:,5:6)=[];M.Y(:,5:6)=[];M.Z(:,5:6)=[];
        [S2,M2]=TrapezoidalPiece3D(Ri_z,Re_z,z,Ai,Ae);
        //the right and left facets are hidden so we can remove them
        S2.X(:,5:6)=[];S2.Y(:,5:6)=[];S2.Z(:,5:6)=[];
        M2.X(:,5:6)=[];M2.Y(:,5:6)=[];M2.Z(:,5:6)=[];
        S=list(S,S2);
        M=list(M,M2);
      else
        S=list(S);
        M=list(M);
      end
    else
      //create shapes for segments:
      // -above spacer; 
      // -spacer 
      // -segment below spacer
     
      Ae=atan(gap_width./Re_z);
      Ai=atan(gap_width./Ri_z);
      m=max(2,ceil(50*max(A)/%pi));
      alphai=linspace(Ai,2*A*ones(Ai),m);
      alphae=linspace(Ae,2*A*ones(Ae),m);      
      //upon spacer
      [Su,Mu]=TrapezoidalPiece3D(Ri_z,Re_z,z,alphai,alphae);
      //above spacer
      [Sa,Ma]=TrapezoidalPiece3D(Ri_z,Re_z,z,-alphai,-alphae);
      //other segments
      alphai=linspace(-A*ones(Ri_z),A*ones(Ri_z),m);
      alphae=linspace(-A*ones(Re_z),A*ones(Re_z),m);
      [Sr,Mr]=TrapezoidalPiece3D(Ri_z,Re_z,z,-alphai,-alphae);
      Sr.X(:,5:6)=[];Sr.Y(:,5:6)=[];Sr.Z(:,5:6)=[];
      Mr.X(:,5:6)=[];Mr.Y(:,5:6)=[];Mr.Z(:,5:6)=[];
      
      if filled then
        Su.X(:,5:6)=[];Su.Y(:,5:6)=[];Su.Z(:,5:6)=[];
        Mu.X(:,5:6)=[];Mu.Y(:,5:6)=[];Mu.Z(:,5:6)=[];
        Sa.X(:,5:6)=[];Sa.Y(:,5:6)=[];Sa.Z(:,5:6)=[];
        Ma.X(:,5:6)=[];Ma.Y(:,5:6)=[];Ma.Z(:,5:6)=[];
        //Spacer
        [Ss,Ms]=TrapezoidalPiece3D(Ri_z,Re_z,z,Ai,Ae);
        Ss.X(:,5:6)=[];Ss.Y(:,5:6)=[];Ss.Z(:,5:6)=[];
        Ms.X(:,5:6)=[];Ms.Y(:,5:6)=[];Ms.Z(:,5:6)=[];
        S=list(Su,Sa,Sr,Ss);M=list(Mu,Ma,Mr,Ms)
      else
        S=list(Su,Sa,Sr);M=list(Mu,Ma,Mr)
      end
    end
  end
  
endfunction




