function  T=ZigzagCuts(lr,Re,Ri,H,RP)

  T=[]
  zigzagprops=RP.ZigzagProps(1)
  N=RP.N;
  A=%pi/N;  
  Le=2*Re.*tan(A);
  Li=2*Ri.*sin(A);
  E=Re-Ri.*cos(A);
  
  
  zh=zigzagprops.zheight
  thickness=zigzagprops.thickness

  thickness=[thickness($:-1:1) thickness(2:$)];
  dh=(H-zh)/2
  
  e=sum(thickness(2:$-1))
  t=(e*zh+Le*sqrt(zh^2+Le^2-e^2))/(zh^2+Le^2);
  az=atan((zh*t-e)/Le,t);
  
  //laminated board length
  Lb=ceil(N*(Le/cos(az)+2)+sum(thickness)*tan(az));
  T=Out(lr,"Zigzag"+string(lr),N,Re,Le,Li,A,H,E,Lb);
endfunction

function T=Out(l,col,n,Re,Le,Li,A,H,E,Lb)
  d=100
  t=2;
  D=round(2*d*Re)/d
  Le=round(d*Le)/d
  Li=round(d*Li)/d
  E=ceil(d*E)/d
  A=round(d*A*180/%pi)
  T=strsubst(msprintf("%i;%s;%g;%i;%g;%g;;%g;%g;%g\n",...
                      l,col,D,n,Le,A,H,E,Lb),".",",");
endfunction
