function PE_Dimensions(win) 
  if type(win)==1 then
    mainH=findobj("figure_id", win);
  else
    mainH=win;
  end
  scf(mainH);
  ud=mainH.user_data
  thick=ud.thickness
  C=ud.C
  D=ud.D
  H=ud.H
  tit=_("TS","Workpiece dimensions");
  
  r=x_mdialog(tit, ...
              [_("TS","Height mm");_("TS","Diameter mm");_("TS","Wall thickness mm")],...
              [sci2exp(H);sci2exp(D);sci2exp(thick)]);
  if r==[] then return;end
  if execstr("r=["+strcat(r,";")+"]","errcatch","m")<>0 then return;end
  H=r(1);
  D=r(2);
  thick=r(3)
  C=ud.C
  if ud.thickness<>thick&C(5).data<>[] then
    ze=C(1).data*[1;%i];
    zz=bezier(ze,40,'o');
    Pe=struct("x",real(zz),"y",imag(zz))
    Pi=InteriorProfile(Pe,r(3))
    C(5).data=[Pi.x;Pi.y]';
  end
  ud.thickness=thick
  ud.D=D
  ud.H=H
  sz=get(0, "screensize_px")(3:4);
  mainH.figure_size=(sz(2)-60)*[D/2/H 1];
  ax=gca();
  ax.data_bounds=[-1 -1;D/2 H];
  mainH.user_data=ud
  mainH.event_handler_enable="on";
  
endfunction
