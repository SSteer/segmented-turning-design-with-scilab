function [X,Y,Z,C]=SegmentedStandardRing3D(D,l)
  RP=D.RingsProperties(l);
  col=colors(RP.Colors(1));
  N=RP.N;
  Re=D.Re(l)+D.Ewidth;
  Ri=max(0,D.Ri(l)-D.Ewidth);
  GapProps=RP.GapProps(1)
  ngap=GapProps.ngap
  gap=GapProps.gap
  typ=convstr(GapProps.type)
  filled=GapProps.filled
 

  h=[0 cumsum(D.H)](l)
  H=D.H(l);
  if N==1 then
    N=4;
  end
  A=%pi/N
  [x,y,z]=Segment3D(Ri,Re,N,H,GapProps)
  gap_index=[]
  X=[];Y=[];Z=[];C=[]
  
 
  if ngap==0|gap==0|typ=="full"  then
    //no gap, all segments have the same shape
    A=%pi/N
    a=0;
    for k=1:N
      [x,y]=Rot2D(x,y,a,[0,0]);
      X=[X x];
      Y=[Y y];
      Z=[Z z];
      a=2*A;
    end
  else
    //rectagular spacer.  
    if ngap==N then
      //Segment3D returns 2 "segments" in X,Y,Z 
      //the first one is the segment 
      //the second one if the spacer
      a=0
      for k=1:N
        [x,y]=Rot2D(x,y,a,[0,0]);
        if filled then gap_index=[gap_index size(X,2)+6+(1:6)];end
        X=[X x];
        Y=[Y y];
        Z=[Z z];
        a=2*%pi/N ;
      end
     
    else
      [xr,yr,zr]=Segment3D(Ri,Re,N,H);
      a=3*%pi/N;b=0;
      for l=1:ngap
        [x,y]=Rot2D(x,y,b)
        if filled then gap_index=[gap_index size(X,2)+6+(1:6)];end
        X=[X x];Y=[Y y];Z=[Z,z];
        for k=1:(N-2*ngap)/ngap     
          [xr,yr]=Rot2D(xr,yr,a)
          X=[X xr];Y=[Y yr];Z=[Z,zr];
          a=2*%pi/N ;
        end
        b=2*%pi/ngap
        a=6*%pi/N ;
      end
      
    end
  end
  Z=Z+h;
  nf=size(X,2);
  ind=1:nf;
  ind(gap_index)=[];
  C=zeros(1,nf);
  C(ind)=matrix(ones(6,1)*col,1,-1);
  if gap_index<>[] then
    C(gap_index)=colors(RP.SpacerColor)
  end
endfunction
