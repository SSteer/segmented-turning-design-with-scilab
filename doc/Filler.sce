Re=108;Ri=91;
N=6;
gap_width=20;
A=%pi/N
[PP,AA]=SegmentPoints(A,Ri,Re,gap_width);

scf(0);clf();ax=gca();
//axes
xpoly([0 PP(1,3)*1.2],[0 PP(2,3)*1.2]);gce().polyline_style=4;
xstring(PP(1,3)*1.2,0,"$\large x$")
xpoly([0 0],[0 PP(2,2)*1.2]);gce().polyline_style=4;
xstring(0,PP(2,2)*1.2,"$\large y$")
xpoly([0 PP(1,2)],[0 PP(2,2)])
xpoly([0 AA(1,3)],[0 AA(2,3)])
xpoly([AA(1,1) PP(1,6)],[AA(2,1) PP(2,6)]);
xpoly([AA(1,1) PP(1,8)],[AA(2,1) PP(2,8)]);

xpoly([PP(1,4) PP(1,1) PP(1,2) PP(1,3) PP(1,4)],[PP(2,4) PP(2,1) PP(2,2) PP(2,3) PP(2,4)]);
xpoly([PP(1,5) PP(1,1) PP(1,2) PP(1,6) PP(1,5)],[PP(2,5) PP(2,1) PP(2,2) PP(2,6) PP(2,5)]);
gce().thickness=3;
//filler
xpoly([PP(1,5) PP(1,6) PP(1,6) PP(1,5) PP(1,5)],[PP(2,5) PP(2,6) -PP(2,6) -PP(2,5) PP(2,5)]);
gce().thickness=3;
xy=[PP(1,5) PP(1,6) PP(1,6) PP(1,5) PP(1,5);PP(2,5) PP(2,6) -PP(2,6) -PP(2,5) PP(2,5)];
xy=rotate(xy,2*A);
xpoly(xy(1,:),xy(2,:))

ax.isoview="on";
ax.data_bounds=[0 -PP(2,5);PP(1,3)*1.2 PP(2,2)*1.2];
ax.margins=0.05*ones(1,4);
gcf().background=-2;


xstring(0,0-5,"$\large O$")
xstring(AA(1,1)-3,AA(2,1),"$\large A_1$")
xstring(AA(1,2),AA(2,2)+2,"$\large A_2$")
xstring(PP(1,5),PP(2,5),"$\large P_5$")
xstring(PP(1,6),PP(2,6),"$\large P_6$")
xstring(AA(1,3),AA(2,3),"$\large A_3$")
xstring(PP(1,1)+2,PP(2,1),"$\large P_1$")
xstring(PP(1,2),PP(2,2),"$\large P_2$")
xstring(PP(1,4),PP(2,4),"$\large P_4$")
xstring(PP(1,3),PP(2,3),"$\large P_3$")
xstring(PP(1,7)+2,PP(2,7)-2,"$\large P_7$")
xstring(PP(1,8),PP(2,8),"$\large P_8$")

t=linspace(0,2*A,30);
xpoly(Ri*cos(t),Ri*sin(t));gce().line_style=2;gce().foreground=5;
xpoly(Re*cos(t),Re*sin(t));gce().line_style=2;gce().foreground=5;
