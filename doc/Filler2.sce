Re=108;Ri=91;
N=6;
A=%pi/N;
gap_width=20;
w=gap_width;
thick=Re-Ri*cos(A);
T=[Re;0];
Q=[Re-thick;0];
U=Ri*[cos(A);sin(A)];
V=Re*[1;tan(A)];
W=Ri*[cos(A);-sin(A)];
X=Re*[1;-tan(A)];
P=(w/2)*[cotg(A);0];
PQ=Re-thick-w/(2*sin(A));

R=[Ri*cos(A);PQ*tan(A)];
PT=Re-w/(2*sin(A));
S=[Re;PT*tan(A)];
Y=[Ri*cos(A);-PQ*tan(A)];
Z=[Re;-PT*tan(A)];
Y1=rotate(Y,2*A);
Z1=rotate(Z,2*A);

scf(4);clf();ax=gca();
xpoly([0 X(1)],[0 X(2)])
xpoly([0 V(1)],[0 V(2)])
xpoly([0 T(1)],[0 T(2)])
xpoly([P(1) S(1)],[P(2) S(2)]);
xpoly([P(1) Z(1)],[P(2) Z(2)]);

xpoly([W(1) U(1) V(1) X(1) W(1)],[W(2) U(2) V(2) X(2) W(2)])
//xpoly([R(1) U(1) V(1) S(1) R(1)],[R(2) U(2) V(2) S(2) R(2)])
//gce().thickness=3;
//xpoly([R(1) S(1) S(1) R(1) R(1)],[R(2) S(2) -S(2) -R(2) R(2)])
//gce().thickness=3;
ax.isoview="on";
ax.data_bounds=[0 X(2);X(1) V(2)];

return

xstring(0,0-5,"O")
xstring(P(1)-2,P(2),"P")
xstring(Q(1),Q(2),"Q")
xstring(R(1),R(2),"R")
xstring(S(1),S(2),"S")
xstring(T(1),T(2),"T")
xstring(U(1)+2,U(2),"U")
xstring(V(1),V(2),"V")
xstring(W(1),W(2),"W")
xstring(X(1),X(2),"X")
xstring(Y(1)-4,Y(2)-2,"Y")
xstring(Z(1),Z(2),"Z")

t=linspace(0,2*A,30);
xpoly(Ri*cos(t),Ri*sin(t));gce().line_style=2;gce().foreground=5;
xpoly(Re*cos(t),Re*sin(t));gce().line_style=2;gce().foreground=5;
