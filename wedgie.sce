N=16;
A=%pi/N;;
a=%pi/2-A;
b=A-a;
C=%pi/2+2*A;
e=5;
clf;
ax=gca();ax.isoview="on";
ax.data_bounds=[-10 -10;10 10];
xpoly([0;0],[10;-10])
xpoly([0;10*cos(A)],[0;10*sin(A)])
xpoly([0;10*cos(A)],-e+[0;-10*sin(A)])
cotea([0,0],[%pi/2,A],2)
cotea([0,-e],[-%pi/2,-A],2)
//cotea([0,0],[A-%pi/2,-%pi/2],1)

alp=-e*cos(A)/(1-2*cos(A)^2);
xpoly([0;alp*sin(A)],[0;-alp*cos(A)])
cotea([alp*sin(A) -alp*cos(A)],[-A,-A+C],2)
